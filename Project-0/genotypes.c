#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

 
GtkBuilder      *builder; 
GtkWidget   	*genotypesWindow;
GtkComboBoxText *genotypes1, *genotypes2;
char  tooltip1[256], tooltip2[256];



typedef struct nodeC{
    char dom;
    char rec;
    char descriptionDom[256];
    char descriptionRec[256];
    struct nodeC * next;
} node_c;

typedef struct nodeG{
    char gen[10];
    struct nodeG * next;
} node_g;

typedef struct nodeP{
    char gen[10];
    double percentage;
    struct nodeP * next;
} node_p;

node_c * nodListC = NULL;
node_g * row = NULL;
node_g * column = NULL;
int total;

void readFile(){
	
	char name[256];
	FILE* fileaux = fopen("name.txt", "r");
	fgets(name, sizeof(name), fileaux);
	

    FILE* file = fopen(name, "r");
    nodListC = NULL;
    char line[256];
	fclose(fileaux);

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    total = atoi(line);
    fgets(line, sizeof(line), file);
    for (int i = 0; i < total*2; i = i + 2){
        if (nodListC == NULL){
            nodListC = malloc(sizeof(node_c));
            nodListC->dom = line[i];
            nodListC->rec = line[i + 1];
            nodListC->next = NULL;
        }else{
            node_c * current = nodListC;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_c));
            current->next->dom = line[i];
            current->next->rec = line[i + 1];
            current->next->next = NULL;
        }
    }
    node_c * current;
    while (fgets(line, sizeof(line), file)) {
        current = nodListC;
        while (current !=NULL){
            if (current->dom == line[0]){
                strcpy(current->descriptionDom,strchr(line,' ')+1);

            }else if (current->rec == line[0]){
                strcpy(current->descriptionRec,strchr(line,' ')+1);
            }
            current = current->next;
        }

    }
    fclose(file);
}

void combine(node_c *characteristics,char (*buffer),int curr,node_g * solution){
    if (characteristics == NULL){
        if (solution == NULL){
            solution = malloc(sizeof(node_g));
            solution->next = NULL;
            strcpy(solution->gen,buffer);
        }
        else{
            node_g * current = solution;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_g));
            strcpy(current->next->gen,buffer);
            current->next->next = NULL;
        }
    }else{
        buffer[curr] = characteristics->dom;
        combine(characteristics->next,buffer,curr + 1,solution);
        buffer[curr] = characteristics->rec;
        combine(characteristics->next,buffer,curr + 1,solution);
    }
}


void combineParents(node_g * row,node_g * column,node_g * solution){
    node_g * currX = row;
    node_g * currY= column;
    while (currX != NULL){
    currY = column;
        while (currY != NULL){
            node_g * current = solution;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_g));
            current->next->gen[total* +2] = '\0';
            int y = 0;
            for (int i = 0;i < total; i++){
                current->next->gen[y] = currX->gen[i];
                y ++;
                current->next->gen[y] = currY->gen[i];
                y ++;
            }
            current->next->next = NULL;
            currY = currY->next;
        }
        currX = currX->next;
    }
}

void combineChar(char * gen,int i,char (*buffer),int curr,node_g * solution){
    if (i >= total*2){
        node_g * current = solution;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(node_g));
        strcpy(current->next->gen,buffer);
        current->next->next = NULL;
    }else{
        buffer[curr] = gen[i];
        combineChar(gen,i+2,buffer,curr + 1,solution);
        buffer[curr] = gen[i+1];
        combineChar(gen,i+2,buffer,curr + 1,solution);
    }
}


void searchDescription(node_c * characteristics, char * gen, char * solution){
    node_c * current;
    strcpy(solution,"Subject : ");
    strcat(solution,gen);
    strcat(solution,"\n");
    strcat(solution,"--Characteristics--");
    strcat(solution,"\n");
    current = characteristics;
    while (current != NULL){
        int i = 0;
        while (i < 10){
            if (gen[i] == current->dom){
                strcat(solution,current->descriptionDom);
                break;
            }else if (gen[i] == current->rec){
                if (gen[i + 1] == current->dom){
                    strcat(solution,current->descriptionDom);
                }else{
                    strcat(solution,current->descriptionRec);
                }
                break;
            }
            i += 2;
        }
        /*
        for (int i = 0;i < 10;i ++){
            if (gen[i] == current->dom){
                strcat(solution,current->descriptionDom);
                break;
            }else if (gen[i] == current->rec){
                strcat(solution,current->descriptionRec);
                break;
            }
        }
        */
        current = current->next;
    }

}

bool isAnagram(char * word1,char * word2){
    int count1[256] = {0};
    int count2[256] = {0};
    int i;
    for (i = 0; word1[i] && word2[i];  i++)
    {
        count1[word1[i]]++;
        count2[word2[i]]++;
    }

    if (word1[i] || word2[i])
      return false;

    for (i = 0; i < 256; i++)
        if (count1[i] != count2[i])
            return false;

    return true;

}

void calculatePoss(node_g * combinations,node_p * solution){
    node_g * current = combinations;
    node_p * check;
    double percentage = (float)(100 / (pow(2,total)*pow(2,total)));
    while (current != NULL){
        check = solution;
        while (check != NULL){
            if (isAnagram(check->gen,current->gen)){
                check->percentage = check->percentage + percentage;
                break;
            }
            if (check->next == NULL){
                check->next = malloc(sizeof(node_p));
                check->next->percentage = percentage;
                strcpy(check->next->gen,current->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        current = current->next;
    }

}


int main(int argc, char *argv[])
{
    
   gtk_init(&argc, &argv);
 
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/mainWindow.glade", NULL);
 
    genotypesWindow = GTK_WIDGET(gtk_builder_get_object(builder, "genotypesWindow"));
    gtk_builder_connect_signals(builder, NULL);
	
	genotypes1 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "genotypes1"));
	genotypes2 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "genotypes2"));
	
	
	
	/////////////////
	
	readFile();

    node_c * current = nodListC;

    char buffer[10] = {'\0'};
    node_g * sons;
    row = malloc(sizeof(node_g));
    row->next = NULL;
    column = malloc(sizeof(node_g));
    column->next = NULL;
    sons = malloc(sizeof(node_g));
    sons->next = NULL;
    combine(nodListC,buffer,0,row);
    combine(nodListC,buffer,0,column);
    row = row->next;
    column = column->next;
	
	combineParents(row,column,sons);
    sons = sons->next;
	
	node_p * solution;
    solution = malloc(sizeof(node_p));
    solution->next = NULL;


    calculatePoss(sons,solution);
    solution = solution->next;
	
	//////////
	/*
	 node_g * x = sons;
    while (x !=NULL){
		
		node_g * y = sons;
    	while (y !=x){
			printf("GenX : %s \n",x->gen);
			printf("GenX : %s \n",y->gen);
			if(strcmp(x->gen,y->gen)==0){
				break;
			}
			if(y->next == x){
				gtk_combo_box_text_insert_text(genotypes1,-1,x->gen);
				gtk_combo_box_text_insert_text(genotypes2,-1,x->gen);
			}
			printf("GenX : %s \n",x->gen);
			
			y = y->next;
    	}
		
        x = x->next;
    }
	*/
	
	//////////////////
	
	node_p * x = solution;
    node_p * newSol;
    newSol = malloc(sizeof(node_p));
    newSol->next = NULL;
    node_p * check = newSol;
    while (x !=NULL){
        while (check != NULL){
            if (isAnagram(check->gen,x->gen)){
                break;
            }
            if (check->next == NULL){
                check->next = malloc(sizeof(node_p));
                check->next->percentage = x->percentage;
                strcpy(check->next->gen,x->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        x = x->next;
    }
    newSol = newSol->next;
    node_p * curr = newSol;
    while (curr !=NULL){
       // printf("Gen Sol : %s Percentage : %f \n",curr->gen,curr->percentage);
		gtk_combo_box_text_insert_text(genotypes1,-1,curr->gen);
		gtk_combo_box_text_insert_text(genotypes2,-1,curr->gen);
        curr = curr->next;
    }
	
	
	
	/*
	gtk_combo_box_text_insert_text(genotypes1,2,"ABCD");
	gtk_combo_box_text_insert_text(genotypes1,1,"ABcd");
	gtk_combo_box_text_insert_text(genotypes1,0,"abcd");
	
	gtk_combo_box_text_insert_text(genotypes2,2,"ABCD");
	gtk_combo_box_text_insert_text(genotypes2,1,"ABcd");
	gtk_combo_box_text_insert_text(genotypes2,0,"abcd");
	*/
	
    g_object_unref(builder);
 
    gtk_widget_show(genotypesWindow);                
    gtk_main();
 
    return 0;
}

//asigna los tooltips
void on_button1_clicked(){
	
	const char *geno1, *geno2;
	geno1 = gtk_combo_box_text_get_active_text(genotypes1);	
	geno2 = gtk_combo_box_text_get_active_text(genotypes2);	
	
	searchDescription(nodListC, geno1, tooltip1);
	searchDescription(nodListC, geno2, tooltip2);
		
	gtk_widget_set_tooltip_text(genotypes1,tooltip1);
	gtk_widget_set_tooltip_text(genotypes2,tooltip2);
	

}

void on_mergeGeno_clicked(){
	
	FILE *f = fopen("parents.txt", "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	
	const char *geno1, *geno2;
	geno1 = gtk_combo_box_text_get_active_text(genotypes1);	
	geno2 = gtk_combo_box_text_get_active_text(genotypes2);	
	
	fprintf(f,geno1);
	fprintf(f,"\n");
	fprintf(f,geno2);
	
	fclose(f);
	
	system("./parentsTable &");
	

}
 


