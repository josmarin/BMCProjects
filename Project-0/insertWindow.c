#include <gtk/gtk.h>
#include <string.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <malloc.h>
 #include <stdbool.h>

GtkBuilder      *builder; 
GtkWidget       *insertWindow;
GtkEntry 		*entry1, *entry2, *entry3, *entry4, *entry5, *entry6, *entry7, *entry8, *entry9, *entry10;
GtkEntry		*file;
GtkEntry *numLetters;
GtkComboBoxText *comboboxtext1, *comboboxtext2, *comboboxtext3, *comboboxtext4, *comboboxtext5, *comboboxtext6, *comboboxtext7, *comboboxtext8, *comboboxtext9,*comboboxtext10 ;

typedef struct nodeC{
    char dom;
    char rec;
    char descriptionDom[256];
    char descriptionRec[256];
    struct nodeC * next;
} node_c;

typedef struct nodeG{
    char gen[10];
    struct nodeG * next;
} node_g;

typedef struct nodeP{
    char gen[10];
    double percentage;
    struct nodeP * next;
} node_p;

node_c * nodListC = NULL;
node_g * row = NULL;
node_g * column = NULL;
int total;

void readFile(char *pFile){

    FILE* file = fopen(pFile, "r");
    nodListC = NULL;
    char line[256];

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    total = atoi(line);
    fgets(line, sizeof(line), file);
    for (int i = 0; i < total*2; i = i + 2){
        if (nodListC == NULL){
            nodListC = malloc(sizeof(node_c));
            nodListC->dom = line[i];
            nodListC->rec = line[i + 1];
            nodListC->next = NULL;
        }else{
            node_c * current = nodListC;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_c));
            current->next->dom = line[i];
            current->next->rec = line[i + 1];
            current->next->next = NULL;
        }
    }
    node_c * current;
    while (fgets(line, sizeof(line), file)) {
        current = nodListC;
        while (current !=NULL){
            if (current->dom == line[0]){
                strcpy(current->descriptionDom,strchr(line,' ')+1);

            }else if (current->rec == line[0]){
                strcpy(current->descriptionRec,strchr(line,' ')+1);
            }
            current = current->next;
        }

    }
    fclose(file);
}

int main(int argc, char *argv[])
{
   
    gtk_init(&argc, &argv);
 
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/mainWindow.glade", NULL);
 
    insertWindow = GTK_WIDGET(gtk_builder_get_object(builder, "insertWindow"));
    gtk_builder_connect_signals(builder, NULL);
	
	//entrys de la descripcion de las caracteristicas
	entry1 = GTK_ENTRY(gtk_builder_get_object(builder, "entry1"));
	entry2 = GTK_ENTRY(gtk_builder_get_object(builder, "entry2"));
	entry3 = GTK_ENTRY(gtk_builder_get_object(builder, "entry3"));
	entry4 = GTK_ENTRY(gtk_builder_get_object(builder, "entry4"));
	entry5 = GTK_ENTRY(gtk_builder_get_object(builder, "entry5"));
	entry6 = GTK_ENTRY(gtk_builder_get_object(builder, "entry6"));
	entry7 = GTK_ENTRY(gtk_builder_get_object(builder, "entry7"));
	entry8 = GTK_ENTRY(gtk_builder_get_object(builder, "entry8"));
	entry9 = GTK_ENTRY(gtk_builder_get_object(builder, "entry9"));
	entry10 = GTK_ENTRY(gtk_builder_get_object(builder, "entry10"));
	
	file = GTK_ENTRY(gtk_builder_get_object(builder, "fileName"));
	
    numLetters = GTK_ENTRY(gtk_builder_get_object(builder, "numLetters"));
	
	// combobox con las letras mayusculas o minusculas
	comboboxtext1 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext1"));
	comboboxtext2 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext2"));
	comboboxtext3 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext3"));
	comboboxtext4 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext4"));
	comboboxtext5 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext5"));
	comboboxtext6 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext6"));
	comboboxtext7 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext7"));
	comboboxtext8 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext8"));
	comboboxtext9 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext9"));
	comboboxtext10 = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "comboboxtext10"));
 
    g_object_unref(builder);
 
    gtk_widget_show(insertWindow);                
    gtk_main();
 
    return 0;
}
 
// called when window is closed
void on_insertWindow_destroy()
{
    gtk_main_quit();
}

void on_button2_clicked(){
	const char * fileName;
	int i=1;
	fileName = gtk_entry_get_text(file);
	
	
	if(strcmp(fileName, "") == 0){
		fileName = "default.txt"	;
	}
	
	readFile(fileName);
	
	char buffer[50];
	char buffer2[50];
	sprintf( buffer, "%d", total);
	
	gtk_entry_set_text(numLetters,buffer);
	on_setLetters_clicked();
	
	 node_c * current = nodListC;
     while (current !=NULL){
		 printf("i: %d",i);
		 strcpy(buffer,current->descriptionDom);
		 buffer[strlen(buffer)-1] = 0;
		 strcpy(buffer2,current->descriptionRec);
		 buffer2[strlen(buffer2)-1] = 0;
		if(i==1){
			gtk_entry_set_text(entry1,buffer);
			gtk_entry_set_text(entry6,buffer2);
		}
		if(i==2){
			gtk_entry_set_text(entry2,buffer);
			gtk_entry_set_text(entry7,buffer2);
		} 
		if(i==3){
			gtk_entry_set_text(entry3,buffer);
			gtk_entry_set_text(entry8,buffer2);
		} 
		if(i==4){
			gtk_entry_set_text(entry4,buffer);
			gtk_entry_set_text(entry9,buffer2);
		} 
		if(i==5){
			gtk_entry_set_text(entry5,buffer);
			gtk_entry_set_text(entry10,buffer2);
		}
     	current = current->next;
		i++;
     }
	
	
}


//Hace visibles la cantidad de caracteristicas
void on_setLetters_clicked()
{
	const char *num ;
	num = gtk_entry_get_text(numLetters);
	//num = gtk_combo_box_text_get_active_text(numLetters);
	
	if(strcmp (num, "1") == 0){
		gtk_widget_set_visible(GTK_ENTRY(entry1),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry6),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext1),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext6),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry2),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry7),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext2),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext7),FALSE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry3),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry8),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext3),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext8),FALSE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry4),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry9),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext4),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext9),FALSE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry5),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry10),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext5),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext10),FALSE );
		
	}
	else if(strcmp (num, "2") == 0){
		gtk_widget_set_visible(GTK_ENTRY(entry1),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry6),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext1),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext6),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry2),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry7),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext2),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext7),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry3),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry8),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext3),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext8),FALSE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry4),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry9),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext4),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext9),FALSE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry5),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry10),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext5),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext10),FALSE );
		
	}
	else if(strcmp (num, "3") == 0){
		gtk_widget_set_visible(GTK_ENTRY(entry1),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry6),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext1),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext6),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry2),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry7),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext2),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext7),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry3),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry8),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext3),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext8),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry4),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry9),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext4),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext9),FALSE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry5),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry10),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext5),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext10),FALSE );
		
	}
	
	else if(strcmp (num, "4") == 0){
		gtk_widget_set_visible(GTK_ENTRY(entry1),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry6),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext1),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext6),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry2),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry7),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext2),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext7),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry3),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry8),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext3),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext8),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry4),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry9),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext4),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext9),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry5),FALSE );
		gtk_widget_set_visible(GTK_ENTRY(entry10),FALSE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext5),FALSE);
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext10),FALSE );
		
	}
	
	else if(strcmp (num, "5") == 0){
		gtk_widget_set_visible(GTK_ENTRY(entry1),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry6),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext1),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext6),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry2),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry7),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext2),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext7),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry3),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry8),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext3),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext8),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry4),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry9),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext4),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext9),TRUE );
		
		gtk_widget_set_visible(GTK_ENTRY(entry5),TRUE );
		gtk_widget_set_visible(GTK_ENTRY(entry10),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext5),TRUE );
		gtk_widget_set_visible(GTK_COMBO_BOX_TEXT(comboboxtext10),TRUE );
		
	}
	
}
//genera el archivo con las caracteristicas ingresadas por el usuario
void on_generateGeno_clicked()
{
	const char *num, *text1,*text2, *text3,*text4 , *text5,*text6 , *text7,*text8, *text9,*text10;
	const char *letter1, *letter2,*letter3, *letter4,*letter5, *letter6,*letter7, *letter8,*letter9, *letter10;
	const char * fileName;
	
	num = gtk_entry_get_text(numLetters);
	
	letter1 = gtk_combo_box_text_get_active_text(comboboxtext1);
	letter2 = gtk_combo_box_text_get_active_text(comboboxtext2);
	letter3 = gtk_combo_box_text_get_active_text(comboboxtext3);
	letter4 = gtk_combo_box_text_get_active_text(comboboxtext4);
	letter5 = gtk_combo_box_text_get_active_text(comboboxtext5);
	letter6 = gtk_combo_box_text_get_active_text(comboboxtext6);
	letter7 = gtk_combo_box_text_get_active_text(comboboxtext7);
	letter8 = gtk_combo_box_text_get_active_text(comboboxtext8);
	letter9 = gtk_combo_box_text_get_active_text(comboboxtext9);
	letter10 = gtk_combo_box_text_get_active_text(comboboxtext10);
	
	text1 = gtk_entry_get_text(entry1);
	text2 = gtk_entry_get_text(entry2);
	text3 = gtk_entry_get_text(entry3);
	text4 = gtk_entry_get_text(entry4);
	text5 = gtk_entry_get_text(entry5);
	text6 = gtk_entry_get_text(entry6);
	text7 = gtk_entry_get_text(entry7);
	text8 = gtk_entry_get_text(entry8);
	text9 = gtk_entry_get_text(entry9);
	text10 = gtk_entry_get_text(entry10);
	
	fileName = gtk_entry_get_text(file);
		
	
	
	
	//nombre del archivo si no se ingresa un nombre
	
	if(strcmp(fileName, "") == 0){
		fileName = "default.txt";
	}
	
	FILE *g = fopen("name.txt", "w");
	fprintf(g,fileName);
	fclose(g);
	
	FILE *f = fopen(fileName, "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	
	fprintf(f,num);
	fprintf(f,"\n");
	
	if(strcmp (num, "1") == 0)
	{
		
		fprintf(f,letter1);
		fprintf(f,letter6);
		fprintf(f,"\n");
		
		
		fprintf(f,letter1);
		fprintf(f," ");
		fprintf(f,text1);
		fprintf(f,"\n");
		fprintf(f,letter6);
		fprintf(f," ");
		fprintf(f,text6);
		fprintf(f,"\n");
		
	}
	
	if(strcmp (num, "2") == 0)
	{
		
		fprintf(f,letter1);
		fprintf(f,letter6);
		fprintf(f,letter2);
		fprintf(f,letter7);
		fprintf(f,"\n");
		
		
		fprintf(f,letter1);
		fprintf(f," ");
		fprintf(f,text1);
		fprintf(f,"\n");
		fprintf(f,letter6);
		fprintf(f," ");
		fprintf(f,text6);
		fprintf(f,"\n");
		
		fprintf(f,letter2);
		fprintf(f," ");
		fprintf(f,text2);
		fprintf(f,"\n");
		fprintf(f,letter7);
		fprintf(f," ");
		fprintf(f,text7);
		fprintf(f,"\n");
		
	}
	
	if(strcmp (num, "3") == 0)
	{
		
		fprintf(f,letter1);
		fprintf(f,letter6);
		fprintf(f,letter2);
		fprintf(f,letter7);
		fprintf(f,letter3);
		fprintf(f,letter8);
		fprintf(f,"\n");
		
		
		fprintf(f,letter1);
		fprintf(f," ");
		fprintf(f,text1);
		fprintf(f,"\n");
		fprintf(f,letter6);
		fprintf(f," ");
		fprintf(f,text6);
		fprintf(f,"\n");
		
		fprintf(f,letter2);
		fprintf(f," ");
		fprintf(f,text2);
		fprintf(f,"\n");
		fprintf(f,letter7);
		fprintf(f," ");
		fprintf(f,text7);
		fprintf(f,"\n");
		
		fprintf(f,letter3);
		fprintf(f," ");
		fprintf(f,text3);
		fprintf(f,"\n");
		fprintf(f,letter8);
		fprintf(f," ");
		fprintf(f,text8);
		fprintf(f,"\n");
		
	}
	
	if(strcmp (num, "4") == 0)
	{
		
		fprintf(f,letter1);
		fprintf(f,letter6);
		fprintf(f,letter2);
		fprintf(f,letter7);
		fprintf(f,letter3);
		fprintf(f,letter8);
		fprintf(f,letter4);
		fprintf(f,letter9);
		fprintf(f,"\n");
		
		
		fprintf(f,letter1);
		fprintf(f," ");
		fprintf(f,text1);
		fprintf(f,"\n");
		fprintf(f,letter6);
		fprintf(f," ");
		fprintf(f,text6);
		fprintf(f,"\n");
		
		fprintf(f,letter2);
		fprintf(f," ");
		fprintf(f,text2);
		fprintf(f,"\n");
		fprintf(f,letter7);
		fprintf(f," ");
		fprintf(f,text7);
		fprintf(f,"\n");
		
		fprintf(f,letter3);
		fprintf(f," ");
		fprintf(f,text3);
		fprintf(f,"\n");
		fprintf(f,letter8);
		fprintf(f," ");
		fprintf(f,text8);
		fprintf(f,"\n");
		
		fprintf(f,letter4);
		fprintf(f," ");
		fprintf(f,text4);
		fprintf(f,"\n");
		fprintf(f,letter9);
		fprintf(f," ");
		fprintf(f,text9);
		fprintf(f,"\n");
		
	}

	if(strcmp (num, "5") == 0)
	{
		
		fprintf(f,letter1);
		fprintf(f,letter6);
		fprintf(f,letter2);
		fprintf(f,letter7);
		fprintf(f,letter3);
		fprintf(f,letter8);
		fprintf(f,letter4);
		fprintf(f,letter9);
		fprintf(f,letter5);
		fprintf(f,letter10);
		fprintf(f,"\n");
		
		
		fprintf(f,letter1);
		fprintf(f," ");
		fprintf(f,text1);
		fprintf(f,"\n");
		fprintf(f,letter6);
		fprintf(f," ");
		fprintf(f,text6);
		fprintf(f,"\n");
		
		fprintf(f,letter2);
		fprintf(f," ");
		fprintf(f,text2);
		fprintf(f,"\n");
		fprintf(f,letter7);
		fprintf(f," ");
		fprintf(f,text7);
		fprintf(f,"\n");
		
		fprintf(f,letter3);
		fprintf(f," ");
		fprintf(f,text3);
		fprintf(f,"\n");
		fprintf(f,letter8);
		fprintf(f," ");
		fprintf(f,text8);
		fprintf(f,"\n");
		
		fprintf(f,letter4);
		fprintf(f," ");
		fprintf(f,text4);
		fprintf(f,"\n");
		fprintf(f,letter9);
		fprintf(f," ");
		fprintf(f,text9);
		fprintf(f,"\n");
		
		fprintf(f,letter5);
		fprintf(f," ");
		fprintf(f,text5);
		fprintf(f,"\n");
		fprintf(f,letter10);
		fprintf(f," ");
		fprintf(f,text10);
		fprintf(f,"\n");
		
	}

	
	fclose(f);
	
	system("./genotypes &");
	
}
