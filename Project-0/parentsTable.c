#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>    
#include <gtk/gtk.h>

GtkBuilder *builder;
GtkBuilder *gtkBuilder;
GtkWidget *windowProb;
GObject *window;
GtkLabel *label;
GtkGrid *grid;
GObject *button, *button2, *button3;

typedef struct nodeC{
    char dom;
    char rec;
    char descriptionDom[256];
    char descriptionRec[256];
    struct nodeC * next;
} node_c;

typedef struct nodeG{
    char gen[11];
	char color[7];
    struct nodeG * next;
} node_g;

typedef struct nodeP{
    char gen[10];
    double percentage;
    struct nodeP * next;
} node_p;

node_c * nodListC = NULL;
node_g * row = NULL;
node_g * column = NULL;
int total;
char probabilities[1024];


//pos es el index en charact, curr es el caracter de la solucion
void combine(node_c *characteristics,char (*buffer),int curr,node_g * solution){
    if (characteristics == NULL){
        if (solution == NULL){
            solution = malloc(sizeof(node_g));
            solution->next = NULL;
            strcpy(solution->gen,buffer);
        }
        else{
            node_g * current = solution;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_g));
            strcpy(current->next->gen,buffer);
            current->next->next = NULL;
        }
    }else{
        buffer[curr] = characteristics->dom;
        combine(characteristics->next,buffer,curr + 1,solution);
        buffer[curr] = characteristics->rec;
        combine(characteristics->next,buffer,curr + 1,solution);
    }
}


void combineChar(char * gen,int i,char (*buffer),int curr,node_g * solution){
    if (i >= total*2){
        node_g * current = solution;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(node_g));
        strcpy(current->next->gen,buffer);
        current->next->next = NULL;
    }else{
        buffer[curr] = gen[i];
        combineChar(gen,i+2,buffer,curr + 1,solution);
        buffer[curr] = gen[i+1];
        combineChar(gen,i+2,buffer,curr + 1,solution);
    }
}

void combineParents(node_g * row,node_g * column,node_g * solution){
    node_g * currX = row;
    node_g * currY= column;
    while (currX != NULL){
    currY = column;
        while (currY != NULL){
            node_g * current = solution;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_g));
<<<<<<< HEAD
            current->next->gen[total*2 +2] = '\0';
=======
            current->next->gen[total*2] = '\0';
>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee
            int y = 0;
            for (int i = 0;i < total; i++){
                current->next->gen[y] = currX->gen[i];
                y ++;
                current->next->gen[y] = currY->gen[i];
                y ++;
            }
			printf("genx:%s\n", currX->gen);
			printf("geny:%s\n", currY->gen);
			printf("gen:%s\n", current->next->gen);
            current->next->next = NULL;
            currY = currY->next;
			
        }
        currX = currX->next;
    }
}

<<<<<<< HEAD
/*
//pos es el index en charact, curr es el caracter de la solucion
void combination(char (*characteristics)[2],int pos,char (*buffer),int curr,char (*solution)[6]){
    if (pos >= arraySize){
        strcpy(solution[ind],buffer);
        ind ++;
    }else{
        buffer[curr] = characteristics[pos][0];
        combination(characteristics,pos + 1,buffer,curr + 1,solution);
        buffer[curr] = characteristics[pos][1];
        combination(characteristics,pos + 1,buffer,curr + 1,solution);
    }
}
*/

=======
>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee

void searchDescription(node_c * characteristics, char * gen, char * solution){
    node_c * current;
    strcpy(solution,"Subject : ");
    strcat(solution,gen);
    strcat(solution,"\n");
    strcat(solution,"--Characteristics--");
    strcat(solution,"\n");
    current = characteristics;
    while (current != NULL){
        int i = 0;
        while (i < 10){
<<<<<<< HEAD
            if (gen[i] == current->dom){
                strcat(solution,current->descriptionDom);
                break;
            }else if (gen[i] == current->rec){
                if (gen[i + 1] == current->dom){
                    strcat(solution,current->descriptionDom);
                }else{
                    strcat(solution,current->descriptionRec);
                }
                break;
            }
            i += 2;
        }
        /*
        for (int i = 0;i < 10;i ++){
=======
>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee
            if (gen[i] == current->dom){
                strcat(solution,current->descriptionDom);
                break;
            }else if (gen[i] == current->rec){
                if (gen[i + 1] == current->dom){
                    strcat(solution,current->descriptionDom);
                }else{
                    strcat(solution,current->descriptionRec);
                }
                break;
            }
            i += 2;
        }
       
        current = current->next;
    }

}


void readFile(){
	
	char name[256];
	FILE* fileaux = fopen("name.txt", "r");
	fgets(name, sizeof(name), fileaux);
	

    FILE* file = fopen(name, "r");

    nodListC = NULL;
    char line[256];

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    total = atoi(line);
    fgets(line, sizeof(line), file);
    for (int i = 0; i < total*2; i = i + 2){
        if (nodListC == NULL){
            nodListC = malloc(sizeof(node_c));
            nodListC->dom = line[i];
            nodListC->rec = line[i + 1];
            nodListC->next = NULL;
        }else{
            node_c * current = nodListC;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_c));
            current->next->dom = line[i];
            current->next->rec = line[i + 1];
            current->next->next = NULL;
        }
    }
    node_c * current;
    while (fgets(line, sizeof(line), file)) {
        current = nodListC;
        while (current !=NULL){
            if (current->dom == line[0]){
                strcpy(current->descriptionDom,strchr(line,' ')+1);

            }else if (current->rec == line[0]){
                strcpy(current->descriptionRec,strchr(line,' ')+1);
            }
            current = current->next;
        }

    }
    fclose(file);
}


void readFileChar(char * word1, char * word2){

    FILE* file = fopen("parents.txt", "r");
    nodListC = NULL;
    char line[256];

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    strcpy(word1,line);
    fgets(line, sizeof(line), file);
    strcpy(word2,line);
    node_c * current;
    fclose(file);
}



bool isAnagram(char * word1,char * word2){
    int count1[256] = {0};
    int count2[256] = {0};
    int i;
    for (i = 0; word1[i] && word2[i];  i++)
    {
        count1[word1[i]]++;
        count2[word2[i]]++;
    }

    if (word1[i] || word2[i])
      return false;

    for (i = 0; i < 256; i++)
        if (count1[i] != count2[i])
            return false;

    return true;

}


void calculatePoss(node_g * combinations,node_p * solution){
    node_g * current = combinations;
    node_p * check;
    double percentage = (float)(100 / (pow(2,total)*pow(2,total)));
    while (current != NULL){
        check = solution;
        while (check != NULL){
            if (isAnagram(check->gen,current->gen)){
                check->percentage = check->percentage + percentage;
                break;
            }
            if (check->next == NULL){
                check->next = malloc(sizeof(node_p));
                check->next->percentage = percentage;
                strcpy(check->next->gen,current->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        current = current->next;
    }

}

void calculatePossFeno(node_g * combinations,node_p * solution){
    node_g * current = combinations;
    node_p * check;
    double percentage = (float)(100 / (pow(2,total)*pow(2,total)));
    while (current != NULL){
        check = solution;
        while (check != NULL){
			char  geno1[256], geno2[256];
			searchDescription(nodListC, check->gen, geno1);
			searchDescription(nodListC, current->gen, geno2);
			//printf("feno1: %s feno2: %s",geno1,geno2);
            if (strcmp(geno1,geno2) == 0){
                check->percentage = check->percentage + percentage;
                break;
            }
            if (check->next == NULL){
                check->next = malloc(sizeof(node_p));
                check->next->percentage = percentage;
                strcpy(check->next->gen,current->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        current = current->next;
    }

}

//crea ventana con resultado de las probabilidades
void showProb (GtkWidget *widget, gpointer   data)
{
	//system("./proba &");
	gtkBuilder = gtk_builder_new();
    gtk_builder_add_from_file (gtkBuilder, "glade/mainWindow.glade", NULL);
    windowProb = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "probWindow"));
    gtk_builder_connect_signals(gtkBuilder, NULL);
 
	char *markup;
	const char *format = "<span letter_spacing='20' font='Roboto 16'>\%s</span>";
	markup = g_markup_printf_escaped (format, probabilities);
	label = GTK_LABEL(gtk_builder_get_object(gtkBuilder, "labelProb"));
	gtk_label_set_markup(label,markup);
	
 	g_object_unref(gtkBuilder);
    gtk_widget_show(windowProb);                
    gtk_main();
}

static void print_geno (GtkWidget *widget, gpointer   data)
{
  g_print ("Hello World\n");
}

static void print_feno (GtkWidget *widget, gpointer   data)
{
  g_print ("Hola Mundo\n");
}

void calculateColor(node_g * combinations){
    node_g * current = combinations;
    node_g * solution = malloc(sizeof(node_g));
    solution->next = NULL;
    node_g * check;
    char bufferC[3]; 
    char color[7];
    char colorT[7];
    while (current != NULL){
        check = solution;
        while (check != NULL){
            if (isAnagram(check->gen,current->gen)){
                strcpy(current->color,check->color);
                break;
            }
            if (check->next == NULL){
                int red = ((rand() % (255 + 1 - 0) + 0) + 255)/2;
                int green = ((rand() % (255 + 1 - 0) + 0) + 255)/2;
                int blue = ((rand() % (255 + 1 - 0) + 0) + 255)/2;

                sprintf(bufferC,"%.2x",red);
                color[0] = bufferC[0];
                color[1] = bufferC[1];
                sprintf(bufferC,"%.2x",green);
                color[2] = bufferC[0];
                color[3] = bufferC[1];
                sprintf(bufferC,"%.2x",blue);
                color[4] = bufferC[0];
                color[5] = bufferC[1];
                color[6] = '\0';

                check->next = malloc(sizeof(node_p));
                strcpy(check->next->color,color);
                strcpy(current->color,check->next->color);
                strcpy(check->next->gen,current->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        */
        current = current->next;
    }

}

/*
for (int i = 0; i < 10; i ++){
        current = characteristics;
        while (characteristics != NULL){
            if (gen[i] == characteristics->dom){
                strcat(solution,current->descriptionDom);
                strcat(solution,"\n");
            }else if (gen[i] == characteristics->rec){
                strcat(solution,current->descriptionRec);
                strcat(solution,"\n");
            }
            current = current->next;
        }
    }
*/


void readFile(){

    FILE* file = fopen("test.txt", "r");
    nodListC = NULL;
    char line[256];

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    total = atoi(line);
    fgets(line, sizeof(line), file);
    for (int i = 0; i < total*2; i = i + 2){
        if (nodListC == NULL){
            nodListC = malloc(sizeof(node_c));
            nodListC->dom = line[i];
            nodListC->rec = line[i + 1];
            nodListC->next = NULL;
        }else{
            node_c * current = nodListC;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_c));
            current->next->dom = line[i];
            current->next->rec = line[i + 1];
            current->next->next = NULL;
        }
    }
    node_c * current;
    while (fgets(line, sizeof(line), file)) {
        current = nodListC;
        while (current !=NULL){
            if (current->dom == line[0]){
                strcpy(current->descriptionDom,strchr(line,' ')+1);

            }else if (current->rec == line[0]){
                strcpy(current->descriptionRec,strchr(line,' ')+1);
            }
            current = current->next;
        }

    }
    fclose(file);
}


void readFileChar(char * word1, char * word2){

    FILE* file = fopen("test2.txt", "r");
    nodListC = NULL;
    char line[256];

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    strcpy(word1,line);
    fgets(line, sizeof(line), file);
    strcpy(word2,line);
    node_c * current;
    fclose(file);
}



bool isAnagram(char * word1,char * word2){
    int count1[256] = {0};
    int count2[256] = {0};
    int i;
    for (i = 0; word1[i] && word2[i];  i++)
    {
        count1[word1[i]]++;
        count2[word2[i]]++;
    }

    if (word1[i] || word2[i])
      return false;

    for (i = 0; i < 256; i++)
        if (count1[i] != count2[i])
            return false;

    return true;

}


void calculatePoss(node_g * combinations,node_p * solution){
    node_g * current = combinations;
    node_p * check;
    double percentage = (float)(100 / (pow(2,total)*pow(2,total)));
    while (current != NULL){
        check = solution;
        while (check != NULL){
            if (isAnagram(check->gen,current->gen)){
                check->percentage = check->percentage + percentage;
                break;
            }
            if (check->next == NULL){
                check->next = malloc(sizeof(node_p));
                check->next->percentage = percentage;
                strcpy(check->next->gen,current->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        current = current->next;
    }
    

}



<<<<<<< HEAD

=======
>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee
int main (int   argc, char *argv[])
{
  
  gtk_init (&argc, &argv);

  /* Construct a GtkBuilder instance and load our UI description */
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, "builder.ui", NULL);

  /* Connect signal handlers to the constructed widgets. */
  window = gtk_builder_get_object (builder, "window");
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
<<<<<<< HEAD

  gtk_window_maximize(GTK_WINDOW(window));
	
  
  
  const char *txt = gtk_label_get_text(GTK_LABEL(label));
	
// poner color	
  const char *format = "<span background=\"blue\" letter_spacing='20' font='Roboto 12'>\%s</span>";
  char *markup;
=======
>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee

 	gtk_window_maximize(GTK_WINDOW(window));
	
	
	 button = gtk_builder_get_object (builder, "buttonProb");
  	 g_signal_connect (button, "clicked", G_CALLBACK (showProb), NULL);
	
	 button2 = gtk_builder_get_object (builder, "buttongeno");
  	 g_signal_connect (button2, "clicked", G_CALLBACK (print_geno), NULL);

	button3 = gtk_builder_get_object (builder, "buttonfeno");
  	 g_signal_connect (button3, "clicked", G_CALLBACK (print_feno), NULL);
	
<<<<<<< HEAD
	///////////////////////////////////////////////////////

    readFile();

=======

    //
	readFile();

>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee
    node_c * current = nodListC;

    char buffer[10] = {'\0'};
    node_g * sons;
    row = malloc(sizeof(node_g));
    row->next = NULL;
    column = malloc(sizeof(node_g));
    column->next = NULL;
    sons = malloc(sizeof(node_g));
    sons->next = NULL;
    //combine(nodListC,buffer,0,row);
    //combine(nodListC,buffer,0,column);
    //row = row->next;
    //column = column->next;
    
    char word1[10];
    char word2[10];
    readFileChar(word1,word2);
    char buffer2[10] = {'\0'};
    combineChar(word1,0,buffer,0,row);
    combineChar(word2,0,buffer,0,column);
    row = row->next;
    column = column->next;
    
    /*
    char * answer[256];
    searchDescription(nodListC,"Aab",answer);
    printf("%s",answer);
    */
<<<<<<< HEAD
    
    node_g * x = row;
    while (x !=NULL){
        printf("GenX : %s \n",x->gen);
        x = x->next;
    }
    node_g * y = column;
    while (y !=NULL){
        printf("GenY : %s \n",y->gen);
        y = y->next;
    }
    

    combineParents(row,column,sons);
    sons = sons->next;

    node_p * solution;
    solution = malloc(sizeof(node_p));
    solution->next = NULL;


    calculatePoss(sons,solution);
    solution = solution->next;

    printf("------------------------Solution------------------------\n");




    node_p * z = solution;
    while (z !=NULL){
        printf("Gen sol : %s\n", z->gen);
        z = z->next;
    }
    


    ///////////////////////////////////////////////
	
		

	node_g * currX = row;
	int k = 2;
	int j = 34;
	while(currX!= NULL){
		
		markup = g_markup_printf_escaped (format, currX->gen);
=======
	
	char *markup;
	char *format = "<span font_weight='bold' letter_spacing='20' font='Roboto 12'>\%s</span>";
  
	//imprime la fila de padres
    int k = 2;
    node_g * x = row;
    while (x !=NULL){
        printf("GenX : %s \n",x->gen);
>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee
		
		markup = g_markup_printf_escaped (format, x->gen);
		//printf("format:%s\n", markup);
			
		char src[50];
		strcpy(src,  "label");
		char buffer[50];
		sprintf( buffer, "%d", k );
		strcat(src,buffer);
		printf("%s",src);
		
		label = GTK_LABEL(gtk_builder_get_object(builder, src));
		gtk_label_set_markup(label,markup);
		gtk_widget_set_visible(label,TRUE );
		
		
		k++;
        x = x->next;
    }
	// imprime la columna de padres
	k = 34;
    node_g * y = column;
    while (y !=NULL){
        printf("GenY : %s \n",y->gen);
		
		markup = g_markup_printf_escaped (format, y->gen);
			
		char src[50];
		strcpy(src,  "label");
		char buffer[50];
		sprintf( buffer, "%d", k );
		strcat(src,buffer);
		
		label = GTK_LABEL(gtk_builder_get_object(builder, src));
		gtk_label_set_markup(GTK_LABEL(label),markup);
		gtk_widget_set_visible(GTK_LABEL(label),TRUE );
		
		k+=33;
		
<<<<<<< HEAD
		j+=33;
		k++;
		currX = currX->next;
	}
	
	
=======
        y = y->next;
    }
    

    combineParents(row,column,sons);
    sons = sons->next;

    node_p * solution;
    solution = malloc(sizeof(node_p));
    solution->next = NULL;

	
    srand(time(NULL));
    calculateColor(sons);


    calculatePoss(sons,solution);
    solution = solution->next;

    printf("------------------------Solution------------------------\n");

	char format2[] = "<span background='#099999' letter_spacing='20' font='Roboto 12'>\%s</span>";

	//imprime el resultado del cruce de los padres
>>>>>>> 7e2ccc8a658bf411ac27ecff98380da3047a16ee
	k = 35;
	int i = 0;
	int j=35;
    node_g * z = sons;
    while (z !=NULL){
        printf("Gen sol : %s\n", z->gen);
		
		format2[19] = z->color[0];
		format2[20] = z->color[1];
		format2[21] = z->color[2];
		format2[22] = z->color[3];
		format2[23] = z->color[4];
		format2[24] = z->color[5];
		
		printf("format: %s", format2);
		
		
		
		markup = g_markup_printf_escaped (format2, z->gen);
		
		char src[50];
		strcpy(src,  "label");
		char buffer[50];
		sprintf( buffer, "%d", j);
		strcat(src,buffer);
		
		
		label = GTK_LABEL(gtk_builder_get_object(builder, src));
		
		gtk_label_set_markup(GTK_LABEL(label),markup);
		gtk_widget_set_visible(GTK_LABEL(label),TRUE );
		
		

		
		j+=33;
		
		if(j==(k+(33*pow(2,total)))){
			k++;
			j=k;
		
		}
		
        z = z->next;
    }
	
	
	//Calcula probabilidades
	z = sons;
	node_p * q = malloc(sizeof(node_p));
	q->next = NULL;
	
	calculatePoss(z, q);
	//calculatePossFeno(z,q);
	q = q->next;
	
	node_p * q1 = q;
	double res = 0;
	
	strcpy(probabilities,  "Probabilidades  \n\n");
	
	
	while(q1 != NULL){
		printf("gen:%s  %.2f\n",q1->gen,q1->percentage);
		
		strcat(probabilities,q1->gen);
		strcat(probabilities,": ");
		
		char buffer[50];
		sprintf( buffer, "%f", q1->percentage);
		
		strcat(probabilities,buffer);
		strcat(probabilities,"\n");
		
		res+=q1->percentage;
		
		q1 = q1->next;
	}
    printf("Prob: %.2f\n",res);
	printf("SOL: \n %s",probabilities);
	
	
	
	
	
	
	/*
	//guarda archivo con las probabilidades
	
	FILE *f = fopen("proba.txt", "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	
	fprintf(f,probabilities);
	
	fclose(f);
	*/
	
	
	
	
	
////////////////////////////
	
	
	
		
  gtk_main ();
	
	
	
	 
	
	
	

  return 0;
}