#include <gtk/gtk.h>
 
GtkBuilder      *builder; 
GtkWidget   	*mainWindow;

int main(int argc, char *argv[])
{
    
   gtk_init(&argc, &argv);
 
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/mainWindow.glade", NULL);
 
    mainWindow = GTK_WIDGET(gtk_builder_get_object(builder, "mainWindow"));
    gtk_builder_connect_signals(builder, NULL);
 
    g_object_unref(builder);
 
    gtk_widget_show(mainWindow);                
    gtk_main();
 
    return 0;
}
 
void on_mainWindow_destroy()
{
    gtk_main_quit();
}

void on_btnInsert_clicked()
{
	system("./insert &");
}

void on_btnExit_clicked(){
	gtk_main_quit();
}
