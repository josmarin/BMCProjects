 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>

typedef struct nodeC{
    char dom;
    char rec;
    char descriptionDom[256];
    char descriptionRec[256];
    struct nodeC * next;
} node_c;

typedef struct nodeG{
    char gen[11];
    char color[7];
    struct nodeG * next;
} node_g;

typedef struct nodeP{
    char gen[10];
    double percentage;
    struct nodeP * next;
} node_p;

node_c * nodListC = NULL;
node_g * row = NULL;
node_g * column = NULL;
int total;


//pos es el index en charact, curr es el caracter de la solucion
void combine(node_c *characteristics,char (*buffer),int curr,node_g * solution){
    if (characteristics == NULL){
        if (solution == NULL){
            solution = malloc(sizeof(node_g));
            solution->next = NULL;
            strcpy(solution->gen,buffer);
        }
        else{
            node_g * current = solution;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_g));
            strcpy(current->next->gen,buffer);
            current->next->next = NULL;
        }
    }else{
        buffer[curr] = characteristics->dom;
        combine(characteristics->next,buffer,curr + 1,solution);
        buffer[curr] = characteristics->rec;
        combine(characteristics->next,buffer,curr + 1,solution);
    }
}


void combineChar(char * gen,int i,char (*buffer),int curr,node_g * solution){
    if (i >= total*2){
        node_g * current = solution;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(node_g));
        strcpy(current->next->gen,buffer);
        current->next->next = NULL;
    }else{
        buffer[curr] = gen[i];
        combineChar(gen,i+2,buffer,curr + 1,solution);
        buffer[curr] = gen[i+1];
        combineChar(gen,i+2,buffer,curr + 1,solution);
    }
}

void combineParents(node_g * row,node_g * column,node_g * solution){
    node_g * currX = row;
    node_g * currY= column;
    while (currX != NULL){
    currY = column;
        while (currY != NULL){
            node_g * current = solution;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_g));
            current->next->gen[total*2 +2] = '\0';
            int y = 0;
            for (int i = 0;i < total; i++){
                current->next->gen[y] = currX->gen[i];
                y ++;
                current->next->gen[y] = currY->gen[i];
                y ++;
            }
            current->next->next = NULL;
            currY = currY->next;
        }
        currX = currX->next;
    }
}

/*
//pos es el index en charact, curr es el caracter de la solucion
void combination(char (*characteristics)[2],int pos,char (*buffer),int curr,char (*solution)[6]){
    if (pos >= arraySize){
        strcpy(solution[ind],buffer);
        ind ++;
    }else{
        buffer[curr] = characteristics[pos][0];
        combination(characteristics,pos + 1,buffer,curr + 1,solution);
        buffer[curr] = characteristics[pos][1];
        combination(characteristics,pos + 1,buffer,curr + 1,solution);
    }
}
*/


void searchDescription(node_c * characteristics, char * gen, char * solution){
    node_c * current;
    strcpy(solution,"Subject : ");
    strcat(solution,gen);
    strcat(solution,"\n");
    strcat(solution,"--Characteristics--");
    strcat(solution,"\n");
    current = characteristics;
    while (current != NULL){
        int i = 0;
        while (i < 10){
            if (gen[i] == current->dom){
                strcat(solution,current->descriptionDom);
                break;
            }else if (gen[i] == current->rec){
                if (gen[i + 1] == current->dom){
                    strcat(solution,current->descriptionDom);
                }else{
                    strcat(solution,current->descriptionRec);
                }
                break;
            }
            i += 2;
        }
        current = current->next;
    }
}




void readFile(){

    FILE* file = fopen("test.txt", "r");
    nodListC = NULL;
    char line[256];

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    total = atoi(line);
    fgets(line, sizeof(line), file);
    for (int i = 0; i < total*2; i = i + 2){
        if (nodListC == NULL){
            nodListC = malloc(sizeof(node_c));
            nodListC->dom = line[i];
            nodListC->rec = line[i + 1];
            nodListC->next = NULL;
        }else{
            node_c * current = nodListC;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(node_c));
            current->next->dom = line[i];
            current->next->rec = line[i + 1];
            current->next->next = NULL;
        }
    }
    node_c * current;
    while (fgets(line, sizeof(line), file)) {
        current = nodListC;
        while (current !=NULL){
            if (current->dom == line[0]){
                strcpy(current->descriptionDom,strchr(line,' ')+1);

            }else if (current->rec == line[0]){
                strcpy(current->descriptionRec,strchr(line,' ')+1);
            }
            current = current->next;
        }

    }
    fclose(file);
}


void readFileChar(char * word1, char * word2){

    FILE* file = fopen("test2.txt", "r");
    nodListC = NULL;
    char line[256];

    //First two lines are the number of characts and the letters for them
    fgets(line, sizeof(line), file);
    strcpy(word1,line);
    fgets(line, sizeof(line), file);
    strcpy(word2,line);
    node_c * current;
    fclose(file);
}



bool isAnagram(char * word1,char * word2){
    int count1[256] = {0};
    int count2[256] = {0};
    int i;
    for (i = 0; word1[i] && word2[i];  i++)
    {
        count1[word1[i]]++;
        count2[word2[i]]++;
    }

    if (word1[i] || word2[i])
      return false;

    for (i = 0; i < 256; i++)
        if (count1[i] != count2[i])
            return false;

    return true;

}


void calculatePoss(node_g * combinations,node_p * solution){
    node_g * current = combinations;
    node_p * check;
    double percentage = (float)(100 / (pow(2,total)*pow(2,total)));
    while (current != NULL){
        check = solution;
        while (check != NULL){
            if (isAnagram(check->gen,current->gen)){
                check->percentage = check->percentage + percentage;
                break;
            }
            if (check->next == NULL){
                check->next = malloc(sizeof(node_p));
                check->next->percentage = percentage;
                strcpy(check->next->gen,current->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        current = current->next;
    }

}

//

void calculateColor(node_g * combinations){
    node_g * current = combinations;
    node_g * solution = malloc(sizeof(node_g));
    solution->next = NULL;
    node_g * check;
    char bufferC[3]; 
    char color[7];
    char colorT[7];
    while (current != NULL){
        check = solution;
        while (check != NULL){
            if (isAnagram(check->gen,current->gen)){
                strcpy(current->color,check->color);
                break;
            }
            if (check->next == NULL){
                int red = ((rand() % (255 + 1 - 0) + 0) + 255)/2;
                int green = ((rand() % (255 + 1 - 0) + 0) + 255)/2;
                int blue = ((rand() % (255 + 1 - 0) + 0) + 255)/2;

                sprintf(bufferC,"%.2x",red);
                color[0] = bufferC[0];
                color[1] = bufferC[1];
                sprintf(bufferC,"%.2x",green);
                color[2] = bufferC[0];
                color[3] = bufferC[1];
                sprintf(bufferC,"%.2x",blue);
                color[4] = bufferC[0];
                color[5] = bufferC[1];
                color[6] = '\0';

                check->next = malloc(sizeof(node_p));
                strcpy(check->next->color,color);
                strcpy(current->color,check->next->color);
                strcpy(check->next->gen,current->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        current = current->next;
    }
    node_g * z = combinations;
    while (z !=NULL){
        printf("Gen sol : %s color : %s \n", z->gen,z->color);
        z = z->next;
    }
    printf("---------\n");

}


//


int main()
{
    /*
    char characteristics [n][2];
    arraySize = sizeof(characteristics)/sizeof(characteristics[0]);
    strcpy(characteristics[0],"Aa");
    strcpy(characteristics[1],"Bb");
    strcpy(characteristics[2],"Cc");
    strcpy(characteristics[3],"Dd");
    char buffer[10] = {'\0'};
    char row[16][6] = {'\0'};
    combination(characteristics,0,buffer,0,row);
    printf("------------------------Solution------------------------\n");
    for (int i = 0; i < (int)pow(2,n); i++){
        printf("row %d : %s \n",i,row[i]);
    }

    //printf("Hello world!\n");
    return 0;
    */

    readFile();

    node_c * current = nodListC;

    char buffer[10] = {'\0'};
    node_g * sons;
    row = malloc(sizeof(node_g));
    row->next = NULL;
    column = malloc(sizeof(node_g));
    column->next = NULL;
    sons = malloc(sizeof(node_g));
    sons->next = NULL;
    //combine(nodListC,buffer,0,row);
    //combine(nodListC,buffer,0,column);
    //row = row->next;
    //column = column->next;
    
    char word1[10];
    char word2[10];
    readFileChar(word1,word2);
    char buffer2[10] = {'\0'};
    combineChar(word1,0,buffer,0,row);
    combineChar(word2,0,buffer,0,column);
    row = row->next;
    column = column->next;
    
    /*
    char * answer[256];
    searchDescription(nodListC,"Aab",answer);
    printf("%s",answer);
    */
    
    node_g * x = row;
    while (x !=NULL){
        printf("GenX : %s \n",x->gen);
        x = x->next;
    }
    node_g * y = column;
    while (y !=NULL){
        printf("GenY : %s \n",y->gen);
        y = y->next;
    }
    

    combineParents(row,column,sons);
    sons = sons->next;

    node_p * solution;
    solution = malloc(sizeof(node_p));
    solution->next = NULL;


    calculatePoss(sons,solution);
    solution = solution->next;

    printf("------------------------Solution------------------------\n");


    srand(time(NULL));
    calculateColor(sons);
    node_g * z = sons;
    while (z !=NULL){
        printf("Gen sol : %s color : %s \n", z->gen,z->color);
        z = z->next;
    }

    /*
    char bufferC[3]; 
    char color[7];

    int red = ((rand() % (255 + 1 - 0) + 0) + 255)/2;
    int green = ((rand() % (255 + 1 - 0) + 0) + 255)/2;
    int blue = ((rand() % (255 + 1 - 0) + 0) + 255)/2;

    sprintf(bufferC,"%.2x",red);
    color[0] = bufferC[0];
    color[1] = bufferC[1];
    sprintf(bufferC,"%.2x",green);
    color[2] = bufferC[0];
    color[3] = bufferC[1];
    sprintf(bufferC,"%.2x",blue);
    color[4] = bufferC[0];
    color[5] = bufferC[1];
    color[6] = '\0';
    printf("color %s \n", color);    


    printf("color %s \n", color);   
    */

    /*

    //THIS !!!
    node_p * x = solution;
    node_p * newSol;
    newSol = malloc(sizeof(node_p));
    newSol->next = NULL;
    node_p * check = newSol;
    while (x !=NULL){
        while (check != NULL){
            if (isAnagram(check->gen,x->gen)){
                break;
            }
            if (check->next == NULL){
                check->next = malloc(sizeof(node_p));
                check->next->percentage = x->percentage;
                strcpy(check->next->gen,x->gen);
                check->next->next = NULL;
                break;
            }
            check = check->next;
        }
        x = x->next;
    }
    newSol = newSol->next;
    node_p * curr = newSol;
    while (curr !=NULL){
        //printf("Gen Sol : %s Percentage : %f \n",curr->gen,curr->percentage);
        printf("Gen Col : %s Percentage : %f \n",curr->gen,curr->percentage);
        curr = curr->next;
    }
    */

    
    

    /*
    node_g * x = sons;
    node_g * y;
    while (x !=NULL){
        y = sons;
        while (y != NULL){
            if (){

            }

            printf("GenX Sol : %s \n",x->gen);
            printf("GenY Sol : %s \n",y->gen);
            printf("Comp : %d \n",strcmp(x->gen,y->gen));
            y = y->next;
        }
        x = x->next;
    }*/



    /*
    node_g * curr = sons;
    while (curr !=NULL){
        printf("Gen Sol : %s \n",curr->gen);
        curr = curr->next;
    }
    */

    /*
    node_g * curr = solution;
    while (curr !=NULL){
        printf("Gen Sol : %s \n",curr->gen);
        curr = curr->next;
    }
    */

    /*
    for (int i = 0; i < 16; i++){
        printf("row %d : %s \n",i,row[i]);
    }

    while (current !=NULL){
        node_c * current = nodListC;
        printf("dom %c \n",current->dom);
        printf("desc %s \n",current->descriptionDom);
        printf("rec %c \n",current->rec);
        printf("desc %s \n",current->descriptionRec);
        current = current->next;
    }
    */

    return 0;

}
