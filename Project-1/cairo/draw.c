//Make sure gtk3-devel is installed (in Fedora #dnf install gtk3-devel)
//To compile:
//gcc draw.c `pkg-config --cflags gtk+-3.0 --libs gtk+-3.0` -o draw
#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//------------------------------------------------------------------
gboolean draw_callback(GtkWidget *widget,cairo_t *cr)
{
guint width,height;
GdkRGBA color;
GtkStyleContext *context;

//cairo_t *cr;
//cr = gdk_cairo_create (gtk_widget_get_window (widget));



//context=gtk_widget_get_style_context(widget);
//width=gtk_widget_get_allocated_width(widget);
//height=gtk_widget_get_allocated_height(widget);
//gtk_render_background(context,cr,600,0,width,height);
//cairo_arc(cr,width/2.0,height/2.0,MIN(width,height)/2.0,0,2*G_PI);

int posGen = 55;
int heightGen = 50;
int wideGen = 20;
int i;
int j;
int numGens = 100;

cairo_set_source_rgb (cr, 0, 0, 255);
cairo_rectangle (cr, 10, 10, posGen, heightGen);
cairo_fill (cr);

for(i=0; i<numGens; i++){

  cairo_set_line_width (cr, 2);
  cairo_set_source_rgb (cr, 255, 0, 0);
  cairo_rectangle (cr, posGen, 10, wideGen, heightGen);
  cairo_fill (cr);
  cairo_stroke (cr);

  char name[10];
  char buffer[10];

  strcpy(name,"G");
  sprintf(buffer,"%d",i);
  strcat(name,buffer);


  cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
  cairo_set_font_size (cr, 10);
  cairo_move_to (cr, posGen+3,40);
  cairo_show_text (cr, name);


  cairo_set_source_rgb (cr, 0, 0, 255);
  cairo_rectangle (cr, posGen+wideGen, 10, 55, heightGen);
  cairo_fill (cr);
  posGen+=75;

}


cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
posGen = 55;

for(i=0; i<numGens; i++){

  //printf("%d\n",posGen);
  heightGen=50;
  int k = 0;
  srand(time(NULL));
  for(j=i;j<numGens-1;j++){


    printf("%d\n  %d\n  %d\n", 1,2,3);

      cairo_set_source_rgb (cr, 0, 0, 0);

    int aux = 45*k;
    int aux2 = 30*k;

    cairo_set_line_width (cr, 1.5);
    cairo_set_font_size (cr, 7);
    cairo_move_to (cr, posGen+10, heightGen+20);
    cairo_line_to (cr, posGen+10, heightGen+40);
    cairo_line_to (cr, posGen+85+aux+aux2, heightGen+40);
    cairo_move_to (cr, posGen+42+aux, heightGen+48);
    cairo_show_text (cr, "0.12");
    //cairo_move_to (cr, posGen+65+aux, heightGen+40);
    //cairo_line_to (cr, posGen+85+aux+aux2, heightGen+40);
    cairo_move_to (cr, posGen+85+aux+aux2, heightGen+40);
    cairo_line_to (cr, posGen+85+aux+aux2, heightGen+20);

    cairo_stroke (cr);
    //heightGen+=10*(numGens-j);
    heightGen+=15;
    k++;
    //posGen+=75;
  }
  posGen += 75;
}


/*


gtk_style_context_get_color(context,gtk_style_context_get_state
(context),&color);
gdk_cairo_set_source_rgba(cr,&color);
gdk_cairo_set_source_rgba(cr,&color);
cairo_fill (cr);*/
return FALSE;
}
//-------------------------------------------------------------------
gint main(int argc,char *argv[])
{
GtkWidget *window,*drawing_area;
gtk_init(&argc,&argv);

GtkBuilder      *builder;

/////////////////////



     /* create window, etc */
     //window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

   builder = gtk_builder_new();
   gtk_builder_add_from_file (builder, "glade/mainWindow.glade", NULL);

   window = GTK_WIDGET(gtk_builder_get_object(builder, "mainWindow"));
   gtk_builder_connect_signals(builder, NULL);
   drawing_area =  GTK_WIDGET(gtk_builder_get_object(builder, "drawingarea1"));
   //g_signal_connect(window,"destroy",G_CALLBACK(gtk_main_quit),NULL);

///////////////////////////////////////////


//////////////////



   g_signal_connect(G_OBJECT(drawing_area),"draw",G_CALLBACK(draw_callback),NULL);
   gtk_widget_show_all(window);
   gtk_main();
return 0;
}
//----------------------------------------------------------------
