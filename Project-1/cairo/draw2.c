#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>

static void do_drawing(cairo_t *, GtkWidget *);

GtkWidget *scale;

static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr,
    gpointer user_data)
{
  do_drawing(cr, widget);

  return FALSE;
}


void on_scale_changed(GtkScaleButton *button, gdouble value, gpointer  user_data){

  printf("%f\n",value);

}

static void do_drawing(cairo_t *cr, GtkWidget *widget)
{
  GtkWidget *win = gtk_widget_get_parent(widget);

  int width, height;
  gtk_window_get_size(GTK_WINDOW(win), &width, &height);

  double value;
  value = gtk_scale_button_get_value (scale);

  int posGen = 55+(value*5);
  int heightGen = 50+(value*5);
  int wideGen = 20+(value*5);
  int i;
  int j;
  int numGens = 10;

  cairo_set_source_rgb (cr, 0, 0, 255);
  cairo_rectangle (cr, 10, 10, posGen, heightGen);
  cairo_fill (cr);

  for(i=0; i<numGens; i++){

    cairo_set_line_width (cr, 2);
    cairo_set_source_rgb (cr, 255, 0, 0);
    cairo_rectangle (cr, posGen, 10, wideGen, heightGen);
    cairo_fill (cr);
    cairo_stroke (cr);

    char name[10];
    char buffer[10];

    strcpy(name,"G");
    sprintf(buffer,"%d",i);
    strcat(name,buffer);


    cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    cairo_set_font_size (cr, 10);
    cairo_move_to (cr, posGen+3,40);
    cairo_show_text (cr, name);


    cairo_set_source_rgb (cr, 0, 0, 255);
    cairo_rectangle (cr, posGen+wideGen, 10, 55+(value*5), heightGen);
    cairo_fill (cr);
    posGen+=75+(value*5);

  }


  cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
  posGen = 55+(value*5);

  //for(i=0; i<numGens; i++){
  for(i=0; i<1; i++){
    //printf("%d\n",posGen);
    heightGen=50+(value*5);
    int k = 0;
    srand(time(NULL));

    for(j=i;j<numGens-1;j++){
    //for(j=i;j<1;j++){

      printf("%d\n  %d\n  %d\n", 1,2,3);

        cairo_set_source_rgb (cr, 0, 0, 0);

      int aux = 45*k+(value*5);
      int aux2 = 30*k+(value*5);

      cairo_set_line_width (cr, 1.5);
      cairo_set_font_size (cr, 7);
      cairo_move_to (cr, posGen+(wideGen/2), heightGen+20);
      cairo_line_to (cr, posGen+(wideGen/2), heightGen+40);
      cairo_line_to (cr, posGen+85+aux+aux2+value, heightGen+40);
      //cairo_move_to (cr, posGen+42+aux, heightGen+48);
    //  cairo_show_text (cr, "0.12");
      //cairo_move_to (cr, posGen+65+aux, heightGen+40);
      //cairo_line_to (cr, posGen+85+aux+aux2, heightGen+40);
      //cairo_move_to (cr, posGen+85+aux+aux2, heightGen+40);
      //cairo_line_to (cr, posGen+85+aux+aux2, heightGen+20);

      cairo_stroke (cr);
      //heightGen+=10*(numGens-j);
      heightGen+=15+value;
      k++;
      //posGen+=75;
    }
    posGen += 75+(value*5);
  }
}


int main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *darea;

  gtk_init(&argc, &argv);

     GtkBuilder      *builder;
     builder = gtk_builder_new();
     gtk_builder_add_from_file (builder, "glade/mainWindow.glade", NULL);

     window = GTK_WIDGET(gtk_builder_get_object(builder, "mainWindow"));
     gtk_builder_connect_signals(builder, NULL);

  darea = GTK_WIDGET(gtk_builder_get_object(builder, "drawingarea1"));
  scale = GTK_WIDGET(gtk_builder_get_object(builder, "scalebutton1"));
  //gtk_container_add(GTK_CONTAINER(window), darea);

  g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL);
  g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
  g_signal_connect(G_OBJECT(scale), "value-changed", G_CALLBACK(on_scale_changed), NULL);

  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 300, 200);
  gtk_window_set_title(GTK_WINDOW(window), "Fill & stroke");

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}
