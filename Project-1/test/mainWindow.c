//#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>

GtkWidget       *spinGenes;
GtkWidget       *treeViewGenes;
GtkWidget       *textBoxName;
GtkWidget       *spinButtonPoss;
GtkWidget       *textBoxDescription;
GtkWidget       *treeViewTable;
GtkWidget       *textBoxFile;
GtkWidget       *ScrolledWindow;
GtkWidget       *ScrolledWindow_Table;
GtkWidget       *Button_Genes;
GtkWidget       *Button_Browse;
GtkWidget       *Button_Save;
GtkWidget       *Button_Load;
GtkWidget       *scale;


GtkBuilder      *builder;
GtkWidget       *mainWindow;
GtkWidget       *Button_Add;
GtkWidget       *Button_Remove;


//==========SRUCTURES==============
typedef struct nodeGene
{
    char     name[100];
    char     description[200];
    struct   nodeGene * next;
}Gene;


enum
{
  COL_NAME,
  COL_DESC,
  COL_GENES_TOTAL
};
//========================
//==========GLOBAL==============
Gene * allGenes;
countGenes = 0;
totalGenes = 0;
double table[1000][1000];
//========================



//==========NODE HANDLING==============
void deleteGene(int pos){
	if (allGenes == NULL){
		printf("Empty genes. By deleteGene(%d).\n",pos);
		return;
	}
	if (pos == 0){
		Gene * current = allGenes;
		if (current->next != NULL){
			allGenes = allGenes->next;
		}else{
			allGenes = NULL;
		}
		free(current);
		totalGenes --;
		return;
	}

	int i = 0;
	Gene * current = allGenes;
	Gene * temp;
    while (i < pos - 1){
    	if (current == NULL){
    		printf("Name not found. By deleteGene(%d).\n",pos);
    		return;
    	}
    	i ++;
        current = current->next;
    }
    if (current->next == NULL){
    	printf("Name not found. By deleteGene(%d).\n",pos);
		return;
    }
    temp = current->next;
	current->next = current->next->next;
	free(temp);
	totalGenes --;
}

void appendGene(){
	char buffer[10];

	if (allGenes == NULL){
		allGenes = malloc(sizeof(Gene));
        strcpy(allGenes->name,"Gene-");
        sprintf(buffer,"%d",countGenes);
        strcat(allGenes->name,buffer);
        strcpy(allGenes->description,"Description here...");
        allGenes->next = NULL;
        //printf("AllGenes=NULL\n");
	}else{
		Gene * current = allGenes;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(Gene));
        strcpy(current->next->name,"Gene-");
        sprintf(buffer,"%d",countGenes);
        strcat(current->next->name,buffer);
        strcpy(current->next->description,"Description here...");
        current->next->next = NULL;
	}
	countGenes ++;
	totalGenes ++;
}



Gene * getLastGene(){

	if (allGenes == NULL){
		printf("Genes empty. By getLastGene().\n");
		return NULL;
	}
	Gene * current = allGenes;
	while(current->next != NULL){
		current = current->next;
	}
	return current;
}

Gene * findGene(int pos){
	Gene * current = allGenes;
	if (current == NULL){
		printf("Pos not found. By findGene(%d).\n",pos);
		return NULL;
	}
	int i = 0;
    while (i < pos){
    	i ++;
        current = current->next;
        if (current == NULL){
    		printf("Pos not found. By findGene(%d).\n",pos);
    		return NULL;
    	}
    }
    return current;
}

void renameGene(int pos,char * pNewName){
	Gene *gene = findGene(pos);
	if (gene == NULL){
		printf("Name not found. By renameGene(%d,%s).\n",pos,pNewName);
		return;
	}
	strcpy(gene->name,pNewName);
}

void printAllGenes(){
	Gene *current = allGenes;
	if (current == NULL){
		printf("Cant Print. By printAllGenes().\n");
		return;
	}
	while (current != NULL){
		printf("==========GENE========\n");
		printf("Name %s\n", current->name);
		printf("Description %s\n", current->description);
		current = current->next;
	}
	printf("=====================\n \n");
}

void clearGenes(){
	for (int i = 0; i < totalGenes; i ++){
		if (findGene(0) == NULL){
			printf("Failed to delete all genes. By clearGenes().\n");
			return;
		}
		deleteGene(0);
	}
}
//==========END NODE HANDLING==============



float stof(const char* s){
  float rez = 0, fact = 1;
  if (*s == '-'){
    s++;
    fact = -1;
  };
  for (int point_seen = 0; *s; s++){
    if (*s == '.'){
      point_seen = 1;
      continue;
    };
    int d = *s - '0';
    if (d >= 0 && d <= 9){
      if (point_seen) fact /= 10.0f;
      rez = rez * 10.0f + (float)d;
    };
  };
  return rez * fact;
};


void printTable(){
    printf("===============\n");
    printf("Total genes : %d\n", totalGenes);
    printf("===============\n");
    for (int i = 0; i < totalGenes;i ++){
        for (int j = 0; j < totalGenes; j++)
        {
            printf("%.2f ",table[i][j]);
        }
        printf("\n");
    }
    printf("===============\n");
}

//==========TREEVIEW HANDLING==============


void cell_edited_callback (GtkCellRendererText *cell,gchar *path_string, gchar *new_text){
    GtkListStore *model = gtk_tree_view_get_model(treeViewTable);
    GtkTreePath *path = gtk_tree_path_new_from_string (path_string);
    GtkTreeIter iter;

    gint column = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cell), "column"));

    gtk_tree_model_get_iter (model, &iter, path);

    gint i;

    i = gtk_tree_path_get_indices (path)[0];

    table[i][column - 1] = stof(new_text);
    table[column - 1][i] = stof(new_text);

    gtk_list_store_set (GTK_LIST_STORE (model), &iter, column,new_text, -1);

}



static void fillTable(){
    GtkCellRenderer *renderer;
    GtkListStore *model;
    GtkTreeIter iter;
    char *buffer[15];
    Gene * current;

    //CLEAR VIEW
   if (gtk_tree_view_get_model((GtkTreeView *)treeViewTable) != NULL){
        model = gtk_tree_view_get_model((GtkTreeView *)treeViewTable);
        int rows = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
        gtk_list_store_clear (model);

        for (; 0 <= rows; rows --){
        	gtk_tree_view_remove_column(treeViewTable,gtk_tree_view_get_column(treeViewTable,rows));
        }
    }

    //GType *types = malloc(totalGenes * sizeof(G_TYPE_STRING));
    GType types[totalGenes * sizeof(G_TYPE_STRING)];
    //printf("Sizeof: %d\n", sizeof(G_TYPE_STRING));

    for (int i = 0; i < totalGenes + 1; i++){
        types[i] = G_TYPE_STRING;
    }

    model = gtk_list_store_newv (totalGenes + 1,types);

    for (int i = 0; i < totalGenes; i++){
    	current = findGene(i);
        gtk_list_store_append (model, &iter);
        gtk_list_store_set (model, &iter,0,current->name,-1);
        for (int j = 1; j < totalGenes + 1; j++){
            sprintf(buffer,"%.2f",table[i][j-1]);
            gtk_list_store_set (model, &iter,j,buffer,-1);
        }
    }

    gtk_tree_view_set_model((GtkTreeView *)treeViewTable,GTK_TREE_MODEL (model));

    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeViewTable),-1, "", renderer,"text", 0,NULL);

    for (int i = 0; i < totalGenes; i++){
    	current = findGene(i);
        renderer = gtk_cell_renderer_text_new ();
        g_object_set (renderer,"editable", TRUE,NULL);
        g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (i + 1));
        g_signal_connect (renderer, "edited",G_CALLBACK (cell_edited_callback),NULL);
        gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeViewTable),-1, current->name,
         	renderer,"text", i + 1,NULL);
    }
    g_object_unref (model);
}






//==========END TREEVIEW TABLE HANDLING==============



//==========TREEVIEW GENES HANDLING==============

static GtkTreeModel * create_genes_model(){
	GtkListStore *model;
	GtkTreeIter iter;

	//CREATE FIRST GENE
	appendGene();

	model = gtk_list_store_new(COL_GENES_TOTAL,G_TYPE_STRING,G_TYPE_STRING);

	gtk_list_store_append (model, &iter);

	Gene *gene = findGene(0);

	gtk_list_store_set (model, &iter,COL_NAME,gene->name,COL_DESC,gene->description,-1);

	return GTK_TREE_MODEL (model);
}


void cell_genes_edited(GtkCellRendererText *cell,const gchar *path_string,
             const gchar *new_text){

	GtkTreeModel *model = gtk_tree_view_get_model((GtkTreeView *)treeViewGenes);
	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
	GtkTreeIter iter;

	gint column = GPOINTER_TO_INT(g_object_get_data (G_OBJECT (cell), "column"));

	gtk_tree_model_get_iter(model, &iter, path);

	switch (column){
		case COL_NAME:{
			gint i;

			i = gtk_tree_path_get_indices(path)[0];

			Gene *gene = findGene(i);
			strcpy(gene->name,new_text);

			gtk_list_store_set (GTK_LIST_STORE (model), &iter, column,new_text, -1);
		}
			break;

		case COL_DESC:{
			gint i;

			i = gtk_tree_path_get_indices(path)[0];

			Gene *gene = findGene(i);
			strcpy(gene->description,new_text);

			gtk_list_store_set (GTK_LIST_STORE (model), &iter, column,new_text, -1);
		}
			break;
	}

	gtk_tree_path_free (path);

	fillTable();
	//printAllGenes();

}


static void add_gene(GtkWidget *button){
	printf("a\n");
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model((GtkTreeView *)treeViewGenes);

	appendGene();
	Gene * gene = getLastGene();
  printf("%s\n",gene->name );

	gtk_list_store_append (GTK_LIST_STORE (model), &iter);
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,COL_NAME, gene->name,
	                  COL_DESC, gene->description,-1);

	fillTable();
	//printAllGenes();
}

static void remove_gene (GtkWidget *widget, gpointer data){

	if (totalGenes == 1){
		return;
	}

	GtkTreeIter iter;
	GtkTreeView *treeview = (GtkTreeView *)data;
	GtkTreeModel *model = gtk_tree_view_get_model (treeview);
	GtkTreeSelection *selection = gtk_tree_view_get_selection (treeview);

	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
	{
		gint i;
		GtkTreePath *path;

		path = gtk_tree_model_get_path (model, &iter);
		i = gtk_tree_path_get_indices (path)[0];
		gtk_list_store_remove (GTK_LIST_STORE (model), &iter);

		gtk_tree_path_free (path);

		for (int x = 0; x < totalGenes; x ++){
			if (x != i){
				table[i][x] = table[i + 1][x];
			}
		}
		for (int y = 0; y < totalGenes; y ++){
			if (y != i){
				table[y][i] = table[y][i + 1];
			}
		}
		if (i != totalGenes){
			table[i][i] = table[i + 1][i + 1];
		}

		deleteGene (i);

		fillTable();
		printAllGenes();
	}
}


void populateListStoreGenes(){
    GtkListStore *model;
    GtkTreeIter iter;
    Gene * current;

    //CLEAR VIEW
    if (gtk_tree_view_get_model((GtkTreeView *)treeViewGenes) != NULL){
        model = gtk_tree_view_get_model((GtkTreeView *)treeViewGenes);
        gtk_list_store_clear (model);
    }

    model = gtk_list_store_new(COL_GENES_TOTAL,G_TYPE_STRING,G_TYPE_STRING);

    for (int i = 0; i < totalGenes; i++){
    	current = findGene(i);
        gtk_list_store_append (model, &iter);
        gtk_list_store_set (model, &iter,COL_NAME,current->name,COL_DESC,current->description,-1);
    }

    gtk_tree_view_set_model((GtkTreeView *)treeViewGenes,GTK_TREE_MODEL (model));

    g_object_unref (model);

}

//==========END TREEVIEW GENES HANDLING==============



//==========FILE HANDLING==============

void on_btn_create_file_clicked(){

	Gene * current;

	char * fileName;
	fileName = gtk_entry_get_text(textBoxFile);

	//default name
	if(strcmp(fileName, "") == 0){
		fileName = "default.txt";
	}

	FILE *f = fopen(fileName, "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}

	char buffer[10];
	sprintf(buffer,"%d",totalGenes);
	fprintf(f,buffer);
	fprintf(f,"\n");

	for (int i = 0; i < totalGenes; i++){
		current = findGene(i);
		fprintf(f,current->name);
		fprintf(f,"\n");
		fprintf(f,current->description);
		fprintf(f,"\n");
	}


  for (int i = 0; i < totalGenes;i ++){
      for (int j = 0; j < totalGenes; j++)
      {
          char buffer[10];
          sprintf(buffer,"%.2f",table[i][j]);
          fprintf(f,buffer);
          fprintf(f," ");
      }
      fprintf(f,"\n");
  }
  fclose(f);
}




void loadFile(){
	Gene *current;
    char fileName[256];
    char line[256];
    FILE* file;

    // //CLEAR GENES
    clearGenes();

    strcpy(fileName,gtk_entry_get_text(textBoxFile));

    file = fopen(fileName, "r");

    fgets(line, sizeof(line), file);
    int totalGenes = atoi(line);
    printf("a\n");
    for (int i = 0; i < totalGenes; i++){
    	appendGene();
    	current = findGene(i);
    	fgets(line, sizeof(line), file);
    	strcpy(current->name,line);
    	fgets(line, sizeof(line), file);
    	strcpy(current->description,line);
    }
    printf("a\n");
    for (int i = 0; i < totalGenes; i++){
        fgets(line, sizeof(line), file);
        char * pch = strtok (line," ");
        for (int j = 0; j < totalGenes;j ++){
            table[i][j] =  stof(pch);
            pch = strtok (NULL, " ");
        }
        printf("\n ");
    }
    printf("a\n");
    countGenes = totalGenes + 1;

    populateListStoreGenes();
    printAllGenes();
    printTable();

    fclose(file);
    fillTable();

}



//==========END FILE HANDLING==============




int main(int argc, char const *argv[])
{
	GtkCellRenderer *renderer;
	GtkTreeModel *genes_model;

	gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "test.glade", NULL);
    gtk_builder_connect_signals(builder, NULL);

    mainWindow = GTK_WIDGET(gtk_builder_get_object(builder, "Window"));
    ScrolledWindow = GTK_WIDGET(gtk_builder_get_object(builder, "ScrolledWindow"));
    ScrolledWindow_Table = GTK_WIDGET(gtk_builder_get_object(builder, "ScrolledWindow_Table"));
    Button_Add = GTK_WIDGET(gtk_builder_get_object(builder, "Button_Add"));
    Button_Remove = GTK_WIDGET(gtk_builder_get_object(builder, "Button_Remove"));

    g_signal_connect (mainWindow, "delete_event", gtk_main_quit, NULL);


    //=======TREEVIEW GENES=========
    genes_model = create_genes_model();

    treeViewGenes = gtk_tree_view_new_with_model (genes_model);

    gtk_container_add (GTK_CONTAINER (ScrolledWindow), treeViewGenes);

	renderer = gtk_cell_renderer_text_new ();

	g_object_set (renderer,"editable", TRUE,NULL);

	g_signal_connect (renderer, "edited",G_CALLBACK (cell_genes_edited), NULL);

	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_NAME));

	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeViewGenes),-1, "Genes",
			 renderer,"text", COL_NAME,NULL);

	renderer = gtk_cell_renderer_text_new ();

	g_object_set (renderer,"editable", TRUE,NULL);

	g_signal_connect (renderer, "edited",G_CALLBACK (cell_genes_edited), NULL);

	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_DESC));

	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeViewGenes),-1, "Description",
			 renderer,"text", COL_DESC,NULL);

	g_object_unref (genes_model);

    //=======TREEVIEW GENES=========
    treeViewTable = gtk_tree_view_new ();
    gtk_container_add (GTK_CONTAINER (ScrolledWindow_Table), treeViewTable);
    fillTable();


    //=======BUTTONS ADD/REMOVE=========
    g_signal_connect (Button_Add, "clicked",G_CALLBACK (add_gene), NULL);
    g_signal_connect (Button_Remove, "clicked",G_CALLBACK (remove_gene), treeViewGenes);
    //================

    //===================. . .===================
    //ENTRY FILE
    textBoxFile = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_file"));

    //===================. . .===================

	//=======BUTTONS ADD/REMOVE=========
    Button_Save = GTK_WIDGET(gtk_builder_get_object(builder, "btn_create_file"));
    g_signal_connect(Button_Save, "clicked",G_CALLBACK(on_btn_create_file_clicked),NULL);
    Button_Load = GTK_WIDGET(gtk_builder_get_object(builder, "Button_Browse"));
    g_signal_connect(Button_Load, "clicked",G_CALLBACK(loadFile),NULL);
    //================

    g_object_unref(builder);
    gtk_widget_show_all (mainWindow);
    gtk_main();

	printf("Success \n");

	return 0;
}
