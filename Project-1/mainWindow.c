#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>

GtkBuilder      *builder;
GtkWidget       *mainWindow;
GtkWidget         *spinGenes;
GtkWidget       *treeViewGenes;
GtkWidget       *textBoxName;
GtkWidget       *spinButtonPoss;
GtkWidget       *textBoxDescription;
GtkWidget       *treeViewTable;
GtkWidget       *textBoxFile;
GtkWidget       *ScrolledWindow;
GtkWidget       *ScrolledWindow_Table;
GtkWidget       *ButtonBox_Genes;
GtkWidget       *ButtonBox_Browse;
GtkWidget       *scale;

//STRUCTS

enum
{
  COL_MAIN = 0
} ;


typedef struct nodeGene
{
    char     name[100];
    char     description[200];
    struct   nodeGene * next;
}
Gene;

typedef struct nodeA{
    int x;
    int y;
    double val;
    char  name[100];
} node_a;

//=============


//GLOBAL
//static cairo_surface_t *surface = NULL;
Gene *currentGene;
Gene *genes = NULL;
gint totalSelectedGenes = 1;
gint countGenes = 1;
double table[1000][1000];
node_a * nodeMat[1000][1000];
bool loading = FALSE;
//=============

void printTable(){
    printf("===============\n");
    printf("Total genes : %d\n", totalSelectedGenes);
    printf("===============\n");
    for (int i = 0; i < totalSelectedGenes;i ++){
        for (int j = 0; j < totalSelectedGenes; j++)
        {
            printf("%.2f ",table[i][j]);
        }
        printf("\n");
    }
    printf("===============\n");
}

void insertionSort(node_a * arr[], int n)
{
   int i, j;
   node_a *key;

   for (i = 1; i < n; i++)
   {
       key = arr[i];
       j = i-1;

       while (j >= 0 && arr[j]->val > key->val)
       {
           arr[j+1] = arr[j];
           j = j-1;
       }
       arr[j+1] = key;
   }
}


float stof(const char* s){
  float rez = 0, fact = 1;
  if (*s == '-'){
    s++;
    fact = -1;
  };
  for (int point_seen = 0; *s; s++){
    if (*s == '.'){
      point_seen = 1;
      continue;
    };
    int d = *s - '0';
    if (d >= 0 && d <= 9){
      if (point_seen) fact /= 10.0f;
      rez = rez * 10.0f + (float)d;
    };
  };
  return rez * fact;
};

float str2float(char* s){
// solve for special cases where s begins with 0's or nun numeric values, or if s is NULL
  float result = 0;
  int decimalCount = 0, i = 0, decimalPointLoc = strlen(s);
  for (; s[i] != '\0'; i++){
    if (s[i] == '.') decimalPointLoc = i;
    if (i < decimalPointLoc) { result *= 10; result += (int)(s[i] + '0'); }
    else { result += (float)(s[i] + '0')/(pow(i-decimalPointLoc,10)); }
  }
  return result;
}

static void fill_combo_genes (GtkWidget *combo){
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "1");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "2");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "3");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "4");
  gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (combo), "5");
}

void cell_edited_callback (GtkCellRendererText *cell,gchar *path_string, gchar *new_text){
    GtkListStore *model = gtk_tree_view_get_model(treeViewTable);
    GtkTreePath *path = gtk_tree_path_new_from_string (path_string);
    GtkTreeIter iter;

    gint column = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cell), "column"));

    gtk_tree_model_get_iter (model, &iter, path);

    gint i;

    i = gtk_tree_path_get_indices (path)[0];

    table[i][column - 1] = stof(new_text);
    table[column - 1][i] = stof(new_text);
    //printf("%.2f \n",table[i][column - 1]);

    gtk_list_store_set (GTK_LIST_STORE (model), &iter, column,new_text, -1);

}


static void fillTable(){
    GtkCellRenderer *renderer;
    GtkListStore *model;
    GtkTreeIter iter;
    GtkTreePath *path;
    char *buffer[15];
    gint rows;


    //CLEAR VIEW
    if (gtk_tree_view_get_model((GtkTreeView *)treeViewTable) != NULL){
        model = gtk_tree_view_get_model((GtkTreeView *)treeViewTable);
        rows = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(model), NULL);
        gtk_list_store_clear (model);
        while (rows > -1){
            gtk_tree_view_remove_column(treeViewTable,gtk_tree_view_get_column(treeViewTable,rows));
            rows --;
        }
    }

    //GType *types = malloc(totalSelectedGenes * sizeof(G_TYPE_STRING));
    GType types[totalSelectedGenes * sizeof(G_TYPE_STRING)];

    for (int i = 0; i < totalSelectedGenes + 1; i++){
        types[i] = G_TYPE_STRING;
    }

    model = gtk_list_store_newv (totalSelectedGenes + 1,types);

    Gene * current = genes;

    for (int i = 0; i < totalSelectedGenes; i++){
        gtk_list_store_append (model, &iter);
        gtk_list_store_set (model, &iter,0,current->name,-1);
        for (int j = 1; j < totalSelectedGenes + 1; j++){
            sprintf(buffer,"%.2f",table[i][j-1]);
            gtk_list_store_set (model, &iter,j,buffer,-1);
        }
        current = current->next;
    }

    gtk_tree_view_set_model((GtkTreeView *)treeViewTable,GTK_TREE_MODEL (model));

    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeViewTable),-1, "", renderer,"text", 0,NULL);

    current = genes;
    for (int i = 0; i < totalSelectedGenes; i++){
        renderer = gtk_cell_renderer_text_new ();
        g_object_set (renderer,"editable", TRUE,NULL);
        g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (i + 1));
        g_signal_connect (renderer, "edited",G_CALLBACK (cell_edited_callback),NULL);
        gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeViewTable),-1, current->name,
         renderer,"text", i + 1,NULL);
        current = current->next;
    }

}





void on_row_selected(GtkWidget *widget,GtkWidget *tree){

    GtkTreeIter iter;
    GtkTreeView *treeview = (GtkTreeView *)tree;
    GtkTreeModel *model = gtk_tree_view_get_model (treeview);
    GtkTreeSelection *selection = gtk_tree_view_get_selection (treeview);

    if (gtk_tree_selection_get_selected (selection, NULL, &iter)){
        gint posGene;
        GtkTreePath *path;

        path = gtk_tree_model_get_path (model, &iter);
        posGene = gtk_tree_path_get_indices (path)[0];

        Gene * current = genes;
        for (int i = 0; i < posGene; i++)
            current = current->next;

        currentGene = current;
        gtk_tree_path_free (path);
        gtk_entry_set_text ((GtkEntry *)textBoxName,current->name);
        gtk_entry_set_text ((GtkEntry *)textBoxDescription,current->description);
    }
}



static void fillgeneTable(){
    GtkListStore *model;
    GtkTreeIter iter;
    GtkTreePath *path;
    char *buffer[15];

    //CLEAR VIEW
    if (gtk_tree_view_get_model((GtkTreeView *)treeViewGenes) != NULL){
        model = gtk_tree_view_get_model((GtkTreeView *)treeViewGenes);
        gtk_list_store_clear (model);
    }

    model = gtk_list_store_new(1,G_TYPE_STRING);

    Gene * current = genes;

    // printf("=======PRINT ON TABLE VALUES=======\n");
    while(current != NULL){
        gtk_list_store_append (model, &iter);
        gtk_list_store_set (model, &iter,0,current->name,-1);
        // printf("Name : %s \n",current->name);
        current = current->next;
    }
    // printf("=======PRINT ON TABLE VALUES=======\n");
    gtk_tree_view_set_model((GtkTreeView *)treeViewGenes,GTK_TREE_MODEL (model));
    fillTable();

}





static void select_gene(GtkWidget *spin,GtkTreeView *tree_view){   //ADDS/REMOVES GENES TO THE TREEVIEW OF GENES (LEFT ONE)
    if (loading)
        return;

    int selectedGenes = gtk_spin_button_get_value_as_int ((GtkSpinButton *)spin);
    gchar buffer[10];
    int prevSelected = totalSelectedGenes;
    int newSelection = selectedGenes;

    if (totalSelectedGenes < selectedGenes){

        for (int i = newSelection - 1;i < newSelection; i ++){
            for (int j = 0; j < newSelection; j ++){
                table[i][j] = 0;
                table[j][i] = 0;
            }
        }

        for (;selectedGenes - totalSelectedGenes != 0; totalSelectedGenes ++){

            Gene * current = genes;
            while (current->next !=NULL){
                current = current->next;
            }
            current->next = malloc(sizeof(Gene));
            strcpy(current->next->name,"Gene-");
            sprintf(buffer,"%d",countGenes);
            strcat(current->next->name,buffer);
            strcpy(current->next->description,"Description here...");
            current->next->next = NULL;

            countGenes ++;
        }
    }else if (totalSelectedGenes > selectedGenes){
        Gene * current = genes;
        int i = 0;
        while (i < selectedGenes-1){
            current = current->next;
            i ++;
        }
        current->next = NULL;
        totalSelectedGenes = selectedGenes;
    }

    fillTable();
    fillgeneTable();

}


static void entry_name_edited(GtkWidget *widget,GtkTreeView *tree_view){
    GtkTreeModel *model = gtk_tree_view_get_model(tree_view);
    GtkTreeSelection *selection = gtk_tree_view_get_selection (tree_view);
    GtkTreeIter iter;

    if (gtk_tree_selection_get_selected (selection, NULL, &iter)){
        gchar *newVal = gtk_entry_get_text((GtkEntry *)widget);
        GtkTreePath *path;

        gtk_list_store_set (GTK_LIST_STORE (model), &iter, 0, newVal, -1);
        strcpy(currentGene->name,newVal);
    }

}


static void entry_description_edited(GtkWidget *widget,GtkTreeView *tree_view){
    gchar *newVal = gtk_entry_get_text((GtkEntry *)widget);
    strcpy(currentGene->description,newVal);
}

/************** save file ****************/
void on_btn_create_file_clicked(){

  Gene * current = genes;

  char * fileName;
  fileName = gtk_entry_get_text(textBoxFile);

  //default name
  if(strcmp(fileName, "") == 0){
        fileName = "default.txt";
  }

  FILE *f = fopen(fileName, "w");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }

  char totalGenes[10];
  sprintf(totalGenes,"%d",totalSelectedGenes);
  fprintf(f,totalGenes);
  fprintf(f,"\n");

  while (current!=NULL){

    fprintf(f,current->name);
    fprintf(f,"\n");
    fprintf(f,current->description);
    fprintf(f,"\n");

    current = current->next;

  }

  for (int i = 0; i < totalSelectedGenes;i ++){
      for (int j = 0; j < totalSelectedGenes; j++)
      {
          char buffer[10];
          sprintf(buffer,"%.2f",table[i][j]);
          //printf("%d ",table[i][j]);
          fprintf(f,buffer);
          fprintf(f,"\n");
      }
      //fprintf(f,"\n");
  }

  fclose(f);

}
/************** end save file ****************/


/**************** draw maps *******************/
static void do_drawing(cairo_t *, GtkWidget *);

static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr,
    gpointer user_data)
{
  do_drawing(cr, widget);

  return FALSE;
}

static void do_drawing(cairo_t *cr, GtkWidget *widget)
{
  GtkWidget *win = gtk_widget_get_parent(widget);

  int width, height;
  gtk_window_get_size(GTK_WINDOW(win), &width, &height);

  double value;
  value = gtk_scale_button_get_value (scale);



  int heightGen = 50+(value);
  int widhtGen = 30+(value);
  int widhtSpace = 60+(value);
  int startX = 10;
  int startY = 10;
  int posGen = widhtSpace+startX;
  int i,j,m,n;
  double cellMax = 99;

//fill matgenes
  for(i = 0; i<totalSelectedGenes; i++){
   for(j = 0; j<totalSelectedGenes; j++){
     //node_a * curr;
     nodeMat[i][j] = malloc(sizeof(node_a));
     nodeMat[i][j]->x = i;
     nodeMat[i][j]->y = j;
     if(table[i][j]==0 && i!=j){
       nodeMat[i][j]->val = cellMax;
     }
     else{
       nodeMat[i][j]->val = table[i][j];
     }
   }
 }
/////////////////

  node_a * temp[1000];
  for(m=0;m<totalSelectedGenes;m++){
    for(n=0;n<totalSelectedGenes;n++){

      temp[n] = malloc(sizeof(node_a));
  		temp[n]->x = n;
  		//printf("%.2f ", nodeMat[m][n]->val);
  		temp[n]->val = nodeMat[m][n]->val;
    }

    Gene * curr = genes;
    n = 0;
    while (curr!=NULL){
      strcpy(temp[n]->name,curr->name);
      curr = curr->next;
      n++;
    }

    printf("Matriz\n");
  	for(i = 0; i<n; i++){
  		for(j = 0; j<n; j++){
  			printf("%.2f ",nodeMat[i][j]->val);
  		}
  		printf("\n");
  	}

      insertionSort(temp,totalSelectedGenes);

      printf("\nSorted\n");
    	for(i = 0; i<n; i++){
    		printf("%.2f ",temp[i]->val);
    	}

    	printf("\nSorted index\n");
    	for(i = 0; i<n; i++){
    		printf("%d ", temp[i]->x);
    	}

      bool draw = TRUE;
      for(i = 1; i<totalSelectedGenes; i++){
        printf("A\n");
    		for(j = i+1; j<totalSelectedGenes; j++){
          printf("B\n");
    			//printf("i: %d j: %d \n %.2f \n",temp[i]->x,temp[j]->x,nodeMat[temp[i]->x][temp[j]->x]->val);
          //printf("i: %d j: %d \n",temp[i]->x,temp[j]->x);
    			//printf("i: %d  j: %d\n %.2f \n",temp[i-1]->x,temp[j]->x,nodeMat[temp[i-1]->x][temp[j]->x]->val);
    			if(((nodeMat[temp[i]->x][temp[j]->x]->val)>(nodeMat[temp[i-1]->x][temp[j]->x]->val)) && nodeMat[temp[i]->x][temp[j]->x]->val != cellMax){
    				draw = FALSE;
            break;
    			}
    		}
    	}

  if(draw){

  cairo_set_source_rgb (cr, 0, 0, 255);
  cairo_rectangle (cr, startX, startY,widhtSpace, heightGen);
  cairo_fill (cr);

  //Gene * current = genes;
  //while (current!=NULL){
      for(i=0;i<totalSelectedGenes;i++){
        cairo_set_line_width (cr, 2);
        cairo_set_source_rgb (cr, 255, 0, 0);
        cairo_rectangle (cr, posGen, startY, widhtGen, heightGen);
        cairo_fill (cr);
        cairo_stroke (cr);

        cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
        cairo_set_font_size (cr, 5+(value/5));
        cairo_move_to (cr, posGen+(widhtGen/6),startY+(heightGen/2));
        cairo_show_text (cr, temp[i]->name);


        cairo_set_source_rgb (cr, 0, 0, 255);
        cairo_rectangle (cr, posGen+widhtGen, startY, widhtSpace, heightGen);
        cairo_fill (cr);
        posGen+=widhtGen+widhtSpace;

        //current = current->next;
  }


  cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
  posGen = widhtSpace+startX;
  int nextMap;

  for(i=0; i<totalSelectedGenes; i++){

      int heightLine = startY+heightGen;
      int k = 1;

      for(j=i+1;j<totalSelectedGenes;j++){

          if(nodeMat[temp[i]->x][temp[j]->x]->val == cellMax){
            continue;
          }
          cairo_set_source_rgb (cr, 0, 0, 0);

          int aux = (widhtGen+widhtSpace)*k;

          cairo_set_line_width (cr, 1.5);
          cairo_set_font_size (cr, 7+(value/5));
          cairo_move_to (cr, posGen+(widhtGen/2), heightLine+20);
          cairo_line_to (cr, posGen+(widhtGen/2), heightLine+40);
          cairo_line_to (cr, posGen+aux+(widhtGen/2), heightLine+40);
          cairo_move_to (cr, posGen+(aux/2), heightLine+48+(value/4));

          char buffer[10];

          sprintf(buffer,"%.2f",nodeMat[temp[i]->x][temp[j]->x]->val);

          cairo_show_text (cr, buffer);

          cairo_move_to (cr, posGen+aux+(widhtGen/2), heightLine+40);
          cairo_line_to (cr, posGen+aux+(widhtGen/2), startY+heightGen+20);

          cairo_stroke (cr);
          heightLine+=value+40;
          k++;
          if(i==0){
              nextMap = heightLine;
          }

        }
   posGen+=widhtGen+widhtSpace;
   }
   startY = nextMap+50;
   posGen = widhtSpace+startX;
  }
 } //end main for

}

void on_scale_changed(GtkScaleButton *button, gdouble value, gpointer  user_data){


}

/**************** end draw maps *******************/


/************** create window maps ****************/
void on_Button_Load_clicked(){

  GtkWidget *window;
  GtkWidget *darea;


  GtkBuilder      *builder;
  builder = gtk_builder_new();
  gtk_builder_add_from_file (builder, "glade/mainWindow.glade", NULL);

  window = GTK_WIDGET(gtk_builder_get_object(builder, "mainWindow"));
  gtk_builder_connect_signals(builder, NULL);

  darea = GTK_WIDGET(gtk_builder_get_object(builder, "drawingarea1"));
  scale = GTK_WIDGET(gtk_builder_get_object(builder, "scalebutton1"));
  //gtk_container_add(GTK_CONTAINER(window), darea);

  g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL);
  g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 300, 200);
  gtk_window_set_title(GTK_WINDOW(window), "Maps");

  gtk_widget_show_all(window);

  gtk_main();

}


/************** end create window maps ****************/

int countArray(char *p){
    int counter = 0;
    while (*p != '\0'){
        counter ++;
        p ++;
    }
    return counter;
}

void loadFile(){

  char fileName[256];
  char line[256];
  FILE* file;


  genes = malloc(sizeof(Gene));
  Gene *current = genes;

  strcpy(fileName,gtk_entry_get_text(textBoxFile));

  if(strcmp(fileName, "") == 0){
      strcpy(fileName,"default.txt");
  }

  file = fopen(fileName, "r");

  if (file == NULL) {
        printf ("File doesn't exist, errno = %d\n", errno);
        return 1;
    }

  fgets(line, sizeof(line), file);
  int total = atoi(line);
  int i = 0;
  int num;
  while(i < total){
    char temp[20];
    current->next = malloc(sizeof(Gene));
    current->next->next = NULL;
    fgets(line, sizeof(line), file);
    strcpy(temp,line);
    num = countArray(temp);
    temp[num-1] = '\0';
    strcpy(current->next->name,temp);
    fgets(line, sizeof(line), file);
    strcpy(current->next->description,line);
    i ++;
    current = current->next;
  }
  current = genes;
  genes = genes->next;
  free(current);

  loading = TRUE;
  gtk_spin_button_set_value (spinGenes,total);
  loading = FALSE;
  totalSelectedGenes = total;
  countGenes = total;
  fillgeneTable();

  for (int i = 0; i < total; i++){
   for (int j = 0; j < total;j ++){
        fgets(line, sizeof(line), file);
        table[i][j] = stof(line);
        //printf("%.2f ", stof(line));
    }
    //printf("\n ");
  }
  fillTable();
  fclose(file);

}



int main(int argc, char *argv[]){

    gtk_init(&argc, &argv);
    GtkTreeSelection *selection;
    GtkCellRenderer *renderer;

    table[0][0] = 0;
    genes = malloc(sizeof(Gene));
    strcpy(genes->name,"Gene-0");
    strcpy(genes->description,"Description here");
    genes->next = NULL;

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/test.glade", NULL);
    gtk_builder_connect_signals(builder, NULL);

    mainWindow = GTK_WIDGET(gtk_builder_get_object(builder, "Window"));
    ScrolledWindow = GTK_WIDGET(gtk_builder_get_object(builder, "ScrolledWindow"));
    ScrolledWindow_Table = GTK_WIDGET(gtk_builder_get_object(builder, "ScrolledWindow_Table"));
    ButtonBox_Genes = GTK_WIDGET(gtk_builder_get_object(builder, "ButtonBox_Genes"));

    textBoxName = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_name"));
    textBoxDescription = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_description"));

    g_signal_connect (mainWindow, "delete_event", gtk_main_quit, NULL);

    //TREEVIEW GENES

    treeViewGenes = gtk_tree_view_new ();
    gtk_container_add (GTK_CONTAINER (ScrolledWindow), treeViewGenes);
    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeViewGenes),-1,"Genes",renderer,"text", 0,NULL);

    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeViewGenes));
    gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
    g_signal_connect (selection,"changed",G_CALLBACK (on_row_selected),treeViewGenes);
    fillgeneTable();
    //===================. . .===================

    //TREEVIEW TABLE

    treeViewTable = gtk_tree_view_new ();
    gtk_container_add (GTK_CONTAINER (ScrolledWindow_Table), treeViewTable);
    fillTable();


    //===================. . .===================


    //COMBO GENES
    spinGenes = GTK_WIDGET(gtk_builder_get_object(builder, "SpinButton_genes"));
    g_signal_connect(spinGenes, "value-changed",G_CALLBACK(select_gene),(GtkTreeView *)treeViewGenes);

    //===================. . .===================

    //ENTRY NAME
    textBoxName = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_name"));
    g_signal_connect(textBoxName, "changed",G_CALLBACK(entry_name_edited),(GtkTreeView *)treeViewGenes);

    //===================. . .===================

    //ENTRY DESCRIPTION
    textBoxDescription = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_description"));
    g_signal_connect(textBoxDescription, "changed",G_CALLBACK(entry_description_edited),(GtkTreeView *)treeViewGenes);

    //===================. . .===================

    //LOAD FILE
    ButtonBox_Browse = GTK_WIDGET(gtk_builder_get_object(builder, "Button_Browse"));
    g_signal_connect(ButtonBox_Browse, "clicked",G_CALLBACK(loadFile),NULL);

    //===================. . .===================
    //ENTRY FILE
    textBoxFile = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_file"));

    //===================. . .===================

    g_object_unref(builder);
    gtk_widget_show_all (mainWindow);
    gtk_main();

    return 0;
}

void on_mainWindow_destroy()
{
    gtk_main_quit();
}

void on_btnExit_clicked(){
    gtk_main_quit();
}
