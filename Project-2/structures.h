#ifndef structure_H_
#define structure_H_

struct cell ; 
struct alignmentD ;

enum name {END = 0, UP,LEFT,DIAGONAL  };

typedef struct cell{
    bool up;
    bool left;
    bool diagonal;
    int  value;
} cellAlig;

typedef struct alignmentD{
    char* firstArray;
    char* secondArray;
    double gap;
    double missmatch;
    double match;
} alignmentData;

typedef struct cellA{
	char *firsArray;
	char *secondArray;
	int totalScore;
} cellAligA;


#endif