#include <stddef.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>

#include "structures.h"
//========GLOBAL=======
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

//===============



//========DATA HANDLING=======



void printTable(cellAlig **result,int rows,int cols){
	printf("===========TABLE===========\n");
	for (int i = 0; i < rows; i++){
		for (int y = 0; y < cols; y++){
			printf("%d ", (*result)[i * cols + y].value);
		}
		printf("\n");
	}
	printf("======================\n");
}


void createTable(cellAlig **result,int rows,int cols){
	*result = malloc(rows * cols * sizeof(*result));
	int offset;

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			offset = i * cols + j;
			(*result)[offset].value = 0;
			(*result)[offset].up = false;
			(*result)[offset].left = false;
			(*result)[offset].diagonal = false;
		}
	}
	return;
}




void calculateAlignmentSW(cellAlig **result,alignmentData *data){

	int rows = strlen(data->firstArray) + 1;
	int cols = strlen(data->secondArray) + 1;
	char *firstArray = data->firstArray;
	char *secondArray = data->secondArray;
	int gapPenalty = data->gap;
	int penalty,up,left,diagonal,max;

	createTable(result,rows,cols);

	for (int i = 1; i < rows; i++){
		for (int j = 1; j < cols; j++){
			if (firstArray[i-1] == secondArray[j-1]){
				penalty = data->match; 
			}else{
				penalty = data->missmatch;
			}

			left = 		(*result)[i * cols + (j-1)].value + data->gap;
			up = 		(*result)[(i - 1) * cols + j].value + data->gap;
			diagonal = 	(*result)[(i - 1) * cols + (j-1)].value + penalty;
			
			//MAX 0
			max = MAX(MAX(MAX(left,up),diagonal),0);
        	(*result)[i * cols + j].value = max;

        	if (max != 0){
        		if (up == max && up != 0) (*result)[(i - 1) * cols + j].up = true;
        		if (left == max && left != 0) (*result)[i * cols + (j-1)].left = true;
        		if (diagonal == max && diagonal != 0) (*result)[(i - 1) * cols + (j-1)].diagonal = true;
        	}

		}
	}
	printTable(result,rows,cols);
}




void backTracSW(alignmentData *data,cellAlig **alignment,char *result){
	int i = data->rows;
	int j , x , y, next;

	while (i > 0){
		j = data->cols;
		while (j > 0){
			x = i;
			y = j;
			while (nextMove(result,x,y) != END){
				next = nextMove(result,i,j);
				if (next == DIAGONAL){
					x = x -1;
					y = y -1;
				}else if (next == UP){
					x = x -1;
				}else if (next == LEFT){
					y = y -1;
				}
			}
			j --;
		}
		i --;
	}
}

int nextMove(cellAlig **alignment,int posX,int posY){
	bool diagonal = (*result)[(i - 1) * cols + (j-1)].diagonal;
	bool left = (*result)[i * cols + (j-1)].left;
	bool up = (*result)[(i - 1) * cols + j].up;
	//DIAGONAl
	if (diagonal){
		return DIAGONAL;
	}
	//UP
	if (up){
		return UP;
	}
	//LEFT
	if (left){
		return LEFT;
	}
	return END;
}





//========DATA HANDLING END=======




//========TESTING=======

int main(int argc, char const *argv[])
{
	cellAlig *result = NULL;

	alignmentData *data = malloc(sizeof(alignmentData));
	data->firstArray = "GTACATTCTA";
	data->secondArray = "ATTGTGATCC";
	data->match = 1;
	data->gap = -2;
	data->missmatch = -1;

	calculateAlignment(&result,data);

	//printTable(&result,10,10);
	
	return 0;
}






