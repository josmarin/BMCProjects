#include <stddef.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>

#include "localAlignment.h"
#include "structures.h"
//========GLOBAL=======


int main(int argc, char const *argv[])
{
	cellAlig *result = NULL;

	alignmentData *data = malloc(sizeof(alignmentData));
	data->firstArray = "GTACATTCTA";
	data->secondArray = "ATTGTGATCC";
	data->match = 1;
	data->gap = -2;
	data->missmatch = -1;

	calculateAlignment(&result,data);

	printTable(&result,11,11);
	
	return 0;
}