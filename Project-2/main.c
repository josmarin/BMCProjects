#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>

typedef struct nodeA{
    int val;
    bool up;
    bool left;
    bool diag;
    bool aligment;
    int dir;
} node_a;

//========GLOBAL=======
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define DEFAULT_FILE "Default.txt"
#define SIZESQUARE   40
#define SIZESPACE    15
#define POS_X_SQUARE 40
#define POS_Y_SQUARE 40



GtkBuilder      *builder;
GtkWidget       *mainWindow;
GtkWidget       *textboxFirstArray;
GtkWidget       *textboxSecondArray;
GtkWidget       *textboxFile;
GtkWidget       *spinMatch;
GtkWidget       *spinMissmatch;
GtkWidget       *spinGap;
GtkWidget       *buttonGenerate;
GtkWidget       *buttonSaveFile;
GtkWidget       *buttonLoadFile;
GtkWidget       *drawing_area;
GtkWidget       *type;
GtkWidget       *messageError;
GtkWidget       *messageError2;
GtkWidget       *messageError3;
GtkWidget       *AligmentFirst;
GtkWidget       *AligmentSecond;
GtkWidget       *AligmentScore;
GtkWidget       *Scoring;

char *format;
char *markup;


gchar    *firstArray;
gchar    *secondArray;
double 	matchValue = 0;
double 	missMatchValue = 0;
double 	gapValue = 0;
node_a **table;
int lenFirstArray;
int lenSecondArray;
int redraw = 1;
gchar *aligmentFirstArray;
gchar *aligmentSecondArray;
gchar *scoreAligment;
int *scoreAligmentInt;
int   score;
int highScoreSW;
int posHighScoreSW[1];

//===============


//======DATA HANDLING==========

int countArray(char *p){
    int counter = 0;
    while (*p != '\0'){
        counter ++;
        p ++;
    }
    return counter;
}


void printAllUserData(){

  printf("=========USER DATA=========\n");
//  printf("First Array : %s\n", firstArray);
  //printf("Second Array : %s\n", secondArray);
  printf("Match value: %.2f \n", matchValue);
  printf("Missmatch value: %.2f \n", missMatchValue);
  printf("Gap value: %.2f \n", gapValue);
  printf("==================\n");

}

char *revStr (char *str) {
    char tmp, *src, *dst;
    size_t len;
    if (str != NULL)
    {
        len = strlen (str);
        if (len > 1) {
            src = str;
            dst = src + len - 1;
            while (src < dst) {
                tmp = *src;
                *src++ = *dst;
                *dst-- = tmp;
            }
        }
    }
    return str;
}

float stof(const char* s){
  float rez = 0, fact = 1;
  if (*s == '-'){
    s++;
    fact = -1;
  };
  for (int point_seen = 0; *s; s++){
    if (*s == '.'){
      point_seen = 1;
      continue;
    };
    int d = *s - '0';
    if (d >= 0 && d <= 9){
      if (point_seen) fact /= 10.0f;
      rez = rez * 10.0f + (float)d;
    };
  };
  return (double)rez * fact;
};

void fill_mat(int pType){

  free(table);

  int x;
  int y;
  //NW
  if (pType==0){
    x = gapValue;
    y = gapValue;
  }
  //SW
  else{
    x = 0;
    y = 0;
  }

  int i,j;
  table = (node_a **) calloc(lenFirstArray, sizeof(node_a *));
  for (i = 0; i<lenFirstArray; ++i){
      table[i] = (node_a *) calloc(lenSecondArray, sizeof (node_a));
  }

  for(i=0;i<lenFirstArray;i++){
    for(j=0;j<lenSecondArray;j++){
      if(i==0&&j!=0){
        table[i][j].val = j*x;
        table[i][j].left = TRUE;
        table[i][j].aligment = FALSE;

      }
      else if(j==0&&i!=0){
        table[i][j].val = i*y;
        table[i][j].up = TRUE;
        table[i][j].aligment = FALSE;
      }
      else{
        table[i][j].val = 0;
        table[i][j].diag = FALSE;
        table[i][j].up = FALSE;
        table[i][j].left = FALSE;
        table[i][j].aligment = FALSE;

      }

    }
  }
}

void Smith_Waterman(){
  int i, j, diag, left, up, max;

  for(i=1;i<lenFirstArray;i++){

    for(j=1;j<lenSecondArray;j++){
        if(firstArray[i-1]==secondArray[j-1]){
            diag = table[i-1][j-1].val + matchValue;
        }
        else{
          diag = table[i-1][j-1].val + missMatchValue;
        }

        left = table[i][j-1].val + gapValue;
        up   = table[i-1][j].val + gapValue;

        max = MAX(MAX(MAX(left,up),diag),0);
        table[i][j].val = max;

        //arrows
        if(max!=0){
          if(max==diag && table[i-1][j-1].val != 0) table[i][j].diag = TRUE;
          if(max==up && table[i-1][j].val != 0) table[i][j].up = TRUE;
          if(max==left && table[i][j-1].val != 0) table[i][j].left = TRUE;
        }
    }
  }

}

void getHighScoreSW(){
  int i,j;
  highScoreSW = 0;
  for(i=0;i<lenFirstArray;i++){
    for(j=0;j<lenSecondArray;j++){
        if(table[i][j].val >= highScoreSW ){
          highScoreSW = table[i][j].val;
          posHighScoreSW[0] = i;
          posHighScoreSW[1] = j;
        }
    }
  }
//  printf("high: %d pos: %d %d",highScoreSW,posHighScoreSW[0],posHighScoreSW[1]);
}


void getAligment_SW(){
  getHighScoreSW();
  char letter[1];
  char buffer[10];
  int i = posHighScoreSW[0];
  int j = posHighScoreSW[1];
  int index = 0;
  aligmentFirstArray = (char*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));
  aligmentSecondArray = (char*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));
  scoreAligment = (char*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));
  scoreAligmentInt = (int*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));

  strcpy(aligmentFirstArray,"\t");
  strcpy(aligmentSecondArray,"\t");
  strcpy(scoreAligment,"\t");

  while(table[i][j].val !=0 ){
    letter[0] = firstArray[i-1];
    letter[1] = '\0';
    strcat(aligmentFirstArray,letter);

    letter[0] = secondArray[j-1];
    letter[1] = '\0';
    strcat(aligmentSecondArray,letter);
    table[i][j].aligment = TRUE;
    table[i][j].dir = 1;

    if(firstArray[i-1] == secondArray[j-1]){
      scoreAligmentInt[index] = (int)matchValue;
    }
    else {
      scoreAligmentInt[index] = (int)missMatchValue;
    }

    strcat(aligmentFirstArray,"\t");
    strcat(aligmentSecondArray,"\t");

    i--;
    j--;
    index++;

  }

  printf("index: %d\n", index);
  index--;
  while(index>=0){
    sprintf(buffer,"%d", scoreAligmentInt[index]);
    strcat(scoreAligment,buffer);
    strcat(scoreAligment,"\t");
    index--;
  }

  //printf("Resp1:%s\nResp2:%s\n", aligmentFirstArray,aligmentSecondArray );


}

void Needleman_Wunsch(){
  int i, j, diag, left, up, max;

  for(i=1;i<lenFirstArray;i++){

    for(j=1;j<lenSecondArray;j++){
        if(firstArray[i-1]==secondArray[j-1]){
            diag = table[i-1][j-1].val + matchValue;
        }
        else{
          diag = table[i-1][j-1].val + missMatchValue;
        }

        left = table[i][j-1].val + gapValue;
        up   = table[i-1][j].val + gapValue;

        max = MAX(MAX(left,up),diag);
        table[i][j].val = max;

        //arrows

        if(max==diag) table[i][j].diag = TRUE;
        if(max==up) table[i][j].up = TRUE;
        if(max==left) table[i][j].left = TRUE;
    }
  }
}


void getAligment_NW(){
  char letter[1];
  char buffer[10];
  int i = lenFirstArray-1, j = lenSecondArray-1;
  int index1 = 0;
  int index2 = 0;
  int index = 0;
  aligmentFirstArray = (char*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));
  aligmentSecondArray = (char*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));
  scoreAligment = (char*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));
  scoreAligmentInt = (int*)malloc(sizeof(char) * ((lenFirstArray+lenSecondArray)*4));
  strcpy(aligmentFirstArray,"\t");
  strcpy(aligmentSecondArray,"\t");
  strcpy(scoreAligment,"\t");

  while(index1 < lenFirstArray-1 || index2 < lenSecondArray-1) {
    //printf("i: %d j: %d\n",i,j);
    if(table[i][j].diag){
      letter[0] = firstArray[i-1];
      letter[1] = '\0';
      strcat(aligmentFirstArray,letter);

      letter[0] = secondArray[j-1];
      strcat(aligmentSecondArray,letter);
      table[i][j].aligment = TRUE;
      table[i][j].dir = 1;


      if(firstArray[i-1] == secondArray[j-1]){
        scoreAligmentInt[index] = (int)matchValue;
      }
      else {
        scoreAligmentInt[index] = (int)missMatchValue;
      }

      i--;
      j--;
      index1++;
      index2++;
    }

    else if(table[i][j].up){
      letter[0] = firstArray[i-1];
      letter[1] = '\0';
      strcat(aligmentFirstArray,letter);
      strcat(aligmentSecondArray,"-");
      table[i][j].aligment = TRUE;
      table[i][j].dir = 2;
      scoreAligmentInt[index] = (int)gapValue;
      i--;
      index1++;
    }
    else if(table[i][j].left){
      strcat(aligmentFirstArray,"-");
      letter[0] = secondArray[j-1];
      letter[1] = '\0';
      strcat(aligmentSecondArray,letter);
      table[i][j].aligment = TRUE;
      table[i][j].dir = 3;
      scoreAligmentInt[index] = (int)gapValue;
      j--;
      index2++;

    }
    strcat(aligmentFirstArray,"\t");
    strcat(aligmentSecondArray,"\t");
    index++;
  } // end while

  table[0][0].aligment = TRUE;

  //reverse array
  printf("index: %d\n", index);
  index--;
  while(index>=0){
    sprintf(buffer,"%d", scoreAligmentInt[index]);
    strcat(scoreAligment,buffer);
    strcat(scoreAligment,"\t");
    index--;
  }

}

void loadData(){

  free(firstArray);
  free(secondArray);

  firstArray = (char*)malloc(sizeof(char) * strlen(gtk_entry_get_text(textboxFirstArray)));
  strcpy(firstArray,gtk_entry_get_text(textboxFirstArray));

  secondArray = (char*)malloc(sizeof(char) * strlen(gtk_entry_get_text(textboxSecondArray)));
  strcpy(secondArray,gtk_entry_get_text(textboxSecondArray));

  matchValue = gtk_spin_button_get_value(spinMatch);
  missMatchValue = gtk_spin_button_get_value(spinMissmatch);
  gapValue = gtk_spin_button_get_value(spinGap);

  lenFirstArray = countArray(firstArray)+1;
  lenSecondArray = countArray(secondArray)+1;

}

//======END DATA HANDLING==========


//======INTERFACE HANDLING==========

void refreshGUI(){
  gtk_entry_set_text ((GtkEntry *)textboxFirstArray,firstArray);
  gtk_entry_set_text ((GtkEntry *)textboxSecondArray,secondArray);

  gtk_spin_button_set_value((GtkSpinButton *)spinMatch,matchValue);
  gtk_spin_button_set_value((GtkSpinButton *)spinMissmatch,missMatchValue);
  gtk_spin_button_set_value((GtkSpinButton *)spinGap,gapValue);

  gtk_entry_set_text ((GtkEntry *)textboxFile,"");
}

gboolean draw_callback(GtkWidget *widget,cairo_t *cr){

  guint width, height;
  GdkRGBA color;
  GtkStyleContext *context;

  context = gtk_widget_get_style_context (widget);

  width = gtk_widget_get_allocated_width (widget);
  height = gtk_widget_get_allocated_height (widget);

  gtk_render_background (context, cr, 0, 0, width, height);

  int posXRec = POS_X_SQUARE;
  int posYRec = POS_Y_SQUARE;
  int SizeRec = SIZESQUARE;
  int spaceRec = SIZESPACE;
  int i,j,x,y;


  for(i=0;i<lenFirstArray;i++){
    for(j=0;j<lenSecondArray;j++){

    }
  }
  for(i=0;i<lenFirstArray;i++){
    for(j=0;j<lenSecondArray;j++){
      cairo_set_source_rgb (cr, 0, 0, 255);
      cairo_rectangle (cr, posXRec, posYRec, SizeRec, SizeRec);
      cairo_stroke (cr);
      char buffer[10];
      sprintf(buffer,"%d",table[i][j].val);
      cairo_move_to (cr, posXRec+SizeRec/2-10, posYRec+SizeRec/2+5);
      cairo_set_source_rgb (cr, 0, 0, 0);
      cairo_set_font_size (cr, 14);
      cairo_show_text (cr, buffer);

      cairo_stroke (cr);
      posXRec+=SizeRec + spaceRec;
      }

      posXRec = POS_X_SQUARE;
      posYRec += SizeRec + spaceRec;
    }

  posXRec = POS_X_SQUARE;
  posYRec = POS_Y_SQUARE;
  SizeRec = SIZESQUARE;
  spaceRec =SIZESPACE;

  char letter[1];

  for(i=0;i<lenFirstArray;i++){
    for(j=0;j<lenSecondArray;j++){


      if(i==0&&j!=0){
        cairo_set_source_rgb (cr, 0, 0, 0);
        if(table[i][j].aligment){
          cairo_set_source_rgb (cr, 255, 0, 0);
        }
        cairo_move_to (cr, posXRec, posYRec+SizeRec/2);
        cairo_line_to (cr, posXRec-spaceRec, posYRec+SizeRec/2);
        cairo_stroke (cr);
        cairo_move_to (cr, posXRec+SizeRec/3, posYRec-10);
        letter[0] = secondArray[j-1];
        letter[1] = '\0';

        cairo_set_source_rgb (cr, 0, 0, 0);
        cairo_show_text (cr, letter);
        cairo_stroke (cr);

      }

      else if(j==0&&i!=0){
        cairo_set_source_rgb (cr, 0, 0, 0);
        if(table[i][j].aligment){
          cairo_set_source_rgb (cr, 255, 0, 0);
        }
        cairo_move_to (cr, posXRec+SizeRec/2, posYRec);
        cairo_line_to (cr, posXRec+SizeRec/2, posYRec-spaceRec);
        cairo_stroke (cr);
        cairo_move_to (cr, posXRec-20,  posYRec+SizeRec/2+2);
        letter[0] = firstArray[i-1];

        cairo_set_source_rgb (cr, 0, 0, 0);
        cairo_show_text (cr, letter);
        cairo_stroke (cr);
      }

      else if (i!=0 && j !=0) {
        if(table[i][j].left){
          cairo_set_source_rgb (cr, 0, 0, 0);
          if(table[i][j].aligment && table[i][j-1].aligment && table[i][j].dir==3){
            cairo_set_source_rgb (cr, 255, 0, 0);
          }

          cairo_move_to (cr, posXRec, posYRec+SizeRec/2);
          cairo_line_to (cr, posXRec-spaceRec, posYRec+SizeRec/2);
          cairo_stroke (cr);
        }

        if(table[i][j].up){
          cairo_set_source_rgb (cr, 0, 0, 0);
          if(table[i][j].aligment && table[i-1][j].aligment && table[i][j].dir==2){
            cairo_set_source_rgb (cr, 255, 0, 0);
          }
          cairo_move_to (cr, posXRec+SizeRec/2, posYRec);
          cairo_line_to (cr, posXRec+SizeRec/2, posYRec-spaceRec);
          cairo_stroke (cr);
      }

        if(table[i][j].diag ){
          cairo_set_source_rgb (cr, 0, 0, 0);
          if(table[i][j].aligment && table[i-1][j-1].aligment && table[i][j].dir==1){
            cairo_set_source_rgb (cr, 255, 0, 0);
          }

          cairo_move_to (cr, posXRec, posYRec);
          cairo_line_to (cr, posXRec-spaceRec, posYRec-spaceRec);
          cairo_stroke (cr);
        }
      }

      cairo_set_source_rgb (cr, 0, 0, 0);
      posXRec+=SizeRec + spaceRec;
    }
    posXRec = POS_X_SQUARE;
    posYRec += SizeRec + spaceRec;
  }

  return FALSE;
}

//======END INTERFACE HANDLING==========



//======FILE HANDLING==========

void saveFile(char *fileName){

  char buffer[25];

  FILE *f = fopen(fileName, "w");
  if (f == NULL)
  {
      printf("Error opening file!\n");
      exit(1);
  }

  fprintf(f,gtk_entry_get_text(textboxFirstArray));
  fprintf(f,"\n");
  fprintf(f,gtk_entry_get_text(textboxSecondArray));
  fprintf(f,"\n");

  sprintf(buffer,"%.2f",gtk_spin_button_get_value(spinMatch));
  fprintf(f,buffer);
  fprintf(f,"\n");

  sprintf(buffer,"%.2f",gtk_spin_button_get_value(spinMissmatch));
  fprintf(f,buffer);
  fprintf(f,"\n");

  sprintf(buffer,"%.2f",gtk_spin_button_get_value(spinGap));
  fprintf(f,buffer);

  fclose(f);

}

void loadFile(char *fileName){
  char line[10000];
  FILE* file;
  int num;

  free(firstArray);
  free(secondArray);

  file = fopen(fileName, "r");

  fgets(line, sizeof(line), file);
  printf("len1:%d",strlen(line));
  firstArray = (char*)malloc(sizeof(char) * strlen(line)*4);
  strcpy(firstArray,line);
  num = countArray(firstArray);
  firstArray[num-1] = '\0';

  fgets(line, sizeof(line), file);
  secondArray = (char*)malloc(sizeof(char) * strlen(line)*4);
  strcpy(secondArray,line);
  num = countArray(secondArray);
  secondArray[num-1] = '\0';

  fgets(line, sizeof(line), file);
  matchValue = stof(line);

  fgets(line, sizeof(line), file);
  missMatchValue = stof(line);
  fgets(line, sizeof(line), file);
  gapValue = stof(line);
  fclose(file);
  printAllUserData();

}

//======END FILE HANDLING==========






//======BUTTON EVENTS==========

void eventSaveFile(){
  if(strcmp(gtk_entry_get_text(textboxFile), "") == 0){
    saveFile(DEFAULT_FILE);
    return;
  }
  saveFile(gtk_entry_get_text(textboxFile));
}


void eventLoadFile(){
  printf("Load\n");
  if(strcmp(gtk_entry_get_text(textboxFile), "") == 0){
    printf("Error: Empty Space for loadFile()\n");
    gtk_widget_set_visible(messageError,TRUE );
    return;
  }
  loadFile(gtk_entry_get_text(textboxFile));
  refreshGUI();
}

void eventGenerate(){

   if(strcmp(gtk_entry_get_text(textboxFirstArray), "") == 0 || strcmp(gtk_entry_get_text(textboxSecondArray), "") == 0){
     gtk_widget_set_visible(messageError2,TRUE );
     return;
   }
   else{
     loadData();
   }

   if(lenFirstArray>10000 || lenSecondArray>10000){
     gtk_widget_set_visible(messageError3,TRUE);
     return;
   }


   if(strcmp(gtk_combo_box_text_get_active_text(type),"Needleman-Wunsch (NW)") == 0){
     fill_mat(0);
     Needleman_Wunsch();
     getAligment_NW();
     score = table[lenFirstArray-1][lenSecondArray-1].val;

   }
   else if(strcmp(gtk_combo_box_text_get_active_text(type),"Smith-Waterman (SW)") == 0){
     fill_mat(1);
     Smith_Waterman();
     getAligment_SW();
     score = highScoreSW;
   }
  printf("score: %d\n",score);

   format = "<span font_weight='bold' background='#0A0' letter_spacing='50' font='Roboto 14'>\%s</span>";
   markup = g_markup_printf_escaped (format, revStr(aligmentFirstArray));
   gtk_label_set_markup (AligmentFirst, markup);
   g_free (markup);

   format = "<span font_weight='bold' background='#CC0' letter_spacing='50' font='Roboto 14'>\%s</span>";
   markup = g_markup_printf_escaped (format, revStr(aligmentSecondArray));
   gtk_label_set_markup (AligmentSecond, markup);
   g_free (markup);

   format = "<span font_weight='bold' background='#A0A' letter_spacing='50' font='Roboto 14'>\%s</span>";
   markup = g_markup_printf_escaped (format, scoreAligment);
   gtk_label_set_markup (AligmentScore, markup);
   g_free (markup);

   gtk_label_set_text (Scoring, " ");
   format = "<span font_weight='bold' foreground='#FFF' background='#00B'  font='Roboto 30'>\%d</span>";
   markup = g_markup_printf_escaped (format,score);
   gtk_label_set_markup (Scoring, markup);
   g_free (markup);

   if(redraw==1){
     redraw = 2;
   }
   else{
     redraw = 1;
   }

   gtk_widget_set_size_request (drawing_area, lenSecondArray*(SIZESQUARE+SIZESPACE)+40+redraw, lenFirstArray*(SIZESQUARE+SIZESPACE)+35);
   g_signal_connect(G_OBJECT(drawing_area),"draw",G_CALLBACK(draw_callback),NULL);
}

void on_button1_clicked(){
  gtk_widget_set_visible(messageError,FALSE );
}
void on_button2_clicked(){
  gtk_widget_set_visible(messageError2,FALSE );
}

void on_button3_clicked(){
  gtk_widget_set_visible(messageError3,FALSE );
}


//======END BUTTON EVENTS==========


int main(int argc, char const *argv[])
{
  gtk_init(&argc, &argv);

  builder = gtk_builder_new();

  gtk_builder_add_from_file (builder, "Glade/mainWindow.glade", NULL);

  gtk_builder_connect_signals(builder, NULL);

  mainWindow = GTK_WIDGET(gtk_builder_get_object(builder, "Window"));
  textboxFirstArray = GTK_WIDGET(gtk_builder_get_object(builder, "Textbox_First_Array"));
  textboxSecondArray = GTK_WIDGET(gtk_builder_get_object(builder, "Textbox_Second_Array"));
  textboxFile = GTK_WIDGET(gtk_builder_get_object(builder, "Textbox_File"));
  type = GTK_WIDGET(gtk_builder_get_object(builder, "type"));
  spinGap = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Gap"));
  spinMatch = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Match"));
  spinMissmatch = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Missmatch"));
  buttonSaveFile = GTK_WIDGET(gtk_builder_get_object(builder, "Button_Save"));
  buttonLoadFile = GTK_WIDGET(gtk_builder_get_object(builder, "Button_Load"));
  buttonGenerate = GTK_WIDGET(gtk_builder_get_object(builder, "Button_Generate"));
  drawing_area =  GTK_WIDGET(gtk_builder_get_object(builder, "drawingarea2"));
  messageError =  GTK_WIDGET(gtk_builder_get_object(builder, "messageError"));
  messageError2 =  GTK_WIDGET(gtk_builder_get_object(builder, "messageError2"));
  messageError3 =  GTK_WIDGET(gtk_builder_get_object(builder, "messageError3"));
  AligmentFirst =  GTK_LABEL(gtk_builder_get_object(builder, "Alingment_First"));
  AligmentSecond =  GTK_LABEL(gtk_builder_get_object(builder,"Alingment_Second"));
  AligmentScore =  GTK_LABEL(gtk_builder_get_object(builder,"Scoring_Aligment"));
  Scoring =  GTK_LABEL(gtk_builder_get_object(builder,"Scoring"));




  g_signal_connect (mainWindow, "delete_event", gtk_main_quit, NULL);

  g_signal_connect(buttonSaveFile, "clicked",G_CALLBACK(eventSaveFile),NULL);
  g_signal_connect(buttonLoadFile, "clicked",G_CALLBACK(eventLoadFile),NULL);
  g_signal_connect(buttonGenerate, "clicked",G_CALLBACK(eventGenerate),NULL);

  g_object_unref(builder);
  gtk_widget_show_all (mainWindow);
  gtk_main();

  free(table);

	return 0;
}
