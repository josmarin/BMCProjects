#ifndef localAlignment_H_ 
#define localAlignment_H_

#include "structures.h"

void printTable(cellAlig **result,int rows,int cols);


void calculateAlignment(cellAlig **result,alignmentData *data);


void findAlignment(alignmentData *data,cellAlig *alignment[],char *result);

#endif // localAlignment_H_