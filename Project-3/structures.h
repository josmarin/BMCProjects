#ifndef structure_H_
#define structure_H_

typedef struct cell{
    int value;
    bool up;
    bool left;
    bool diagonal;
    bool aligment;
} body_cell;

typedef struct alignmentData{
    char* word1;
    char* word2;
    int gap;
    int missMatch;
    int match;
} body_aligment;

typedef struct tuple{
    char *value1;
    char *value2;
} body_tuple;


#endif