#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "commons.h"

#define WEIGHTSFILE "conf/weights.txt"

int getDefaultWeights(int *match,int *missMatch, int *gap){
    char fileName[32];
    char line[32];
    int value;
    FILE* file;

    strcpy(fileName,WEIGHTSFILE);

    file = fopen(fileName, "r");

    if (file == 0 ){
        printf("\n========= ERROR READING ON PAGES ========= \n");
        return -1;
    }

    fgets(line, sizeof(line), file);

    (*match) = atoi(line);

    fgets(line, sizeof(line), file);

    (*missMatch) = atoi(line);

    fgets(line, sizeof(line), file);

    (*gap) = atoi(line);

    fclose(file);

    return value;
}