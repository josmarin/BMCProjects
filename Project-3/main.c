#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

#include "structures.h"
#include "commons.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))


//====================GLOBAL====================
GtkBuilder      *builder;
GtkWidget       *mainWindow;
GtkWidget       *spinMatch;
GtkWidget       *spinMissmatch;
GtkWidget       *spinGap;
GtkWidget       *AligmentFirst;
GtkWidget       *AligmentSecond;
GtkWidget       *AligmentScore;
GtkWidget       *Scoring;
GtkWidget       *AligmentFirst1;
GtkWidget       *AligmentSecond1;
GtkWidget       *AligmentScore1;
GtkWidget       *Scoring1;
GtkWidget       *size_word1;
GtkWidget       *size_word2;

GtkWidget       *Label_timeNW;
GtkWidget       *Label_timeNWL;

GtkWidget       *Label_MemoryNW;
GtkWidget       *Label_MemoryNWL;

char *alphabet = "ATCG";

 double memoryCount;

 clock_t startNW, endNW;
 clock_t startNWL, endNWL;

//====================END GLOBAL====================


int countArray(char *p){
    int counter = 0;
    while (*p != '\0'){
        counter ++;
        p ++;
    }
    return counter;
}

char * addTabs (char *str, int size1, int size2) {

	int i = 0;
	char letter[1];
	gchar *aligmentTemp;
	aligmentTemp = (char*)malloc(sizeof(char) * ((size1 + size2) * 4));
	strcpy(aligmentTemp,"\t");
	for(i=0;i<countArray(str);i++){
		letter[0] = str[i];
		letter[1] = '\0';
		strcat(aligmentTemp,letter);
		strcat(aligmentTemp,"\t");
	}
	strcat(aligmentTemp,"\0");
	return aligmentTemp;

}

char * getScoreAligment(char *str1, char *str2, int match, int miss, int gap){
	int i;
	char letter[1];
	char buffer[10];
	gchar *aligmentTemp;
	aligmentTemp = (char*)malloc(sizeof(char) * ((countArray(str1)+countArray(str2)) * 4));
	strcpy(aligmentTemp,"\t");
	for(i = 0; i<countArray(str1); i++){
		if(str1[i] == '-' || str2[i] == '-'){
			sprintf(buffer,"%d", gap);
		}
		else if(str1[i] == str2[i]){
			sprintf(buffer,"%d", match);
		}
		else{
			sprintf(buffer,"%d", miss);
		}
		strcat(aligmentTemp,buffer);
    strcat(aligmentTemp,"\t");
	}

	strcat(aligmentTemp,"\0");
	printf("score: %s\n", aligmentTemp);

	return aligmentTemp;

}

int getScoring(char *str1, char *str2, int match, int miss, int gap){
	int score = 0;
	int i;
	for(i = 0; i<countArray(str1); i++){
		if(str1[i] == '-' || str2[i] == '-'){
			score += gap;
		}
		else if(str1[i] == str2[i]){
			score += match;
		}
		else{
			score += miss;
		}
	}
	return score;
}



//====================NEEDLEMAN BUNCH===============

void printTable(body_cell **result,int rows,int cols){
	printf("===========TABLE===========\n");
	for (int i = 0; i < rows; i++){
		for (int y = 0; y < cols; y++){
			printf("%d ", (*result)[i * cols + y].value);
		}
		printf("\n");
	}
	printf("======================\n");
}



void createTable(body_cell **result,int rows,int cols,int gap){
	*result = malloc(rows * cols * sizeof(*result));
	int offset,preview;

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			offset = i * cols + j;
			(*result)[offset].value = 0;
			(*result)[offset].up = false;
			(*result)[offset].left = false;
			(*result)[offset].diagonal = false;
		}
	}
	preview = 0;
	for (int i = 0; i < rows; i++){
		offset = i * cols;
		(*result)[offset].up = true;
		(*result)[offset].value = preview;
		preview += gap;
	}
	preview = 0;
	for (int j = 0; j < cols; j++){
		offset = 0 * cols + j;
		(*result)[offset].left = true;
		(*result)[offset].value = preview;
		preview += gap;
	}
	return;
}


void getNeedlemanWunsch(body_cell **result,body_aligment *data){

	int rows = strlen(data->word1) + 1;
	int cols = strlen(data->word2) + 1;
	char *firstArray = data->word1;
	char *secondArray = data->word2;
	int gapPenalty = data->gap;
	int penalty,up,left,diagonal,max;

	memoryCount += rows * cols * sizeof(body_cell);

	createTable(result,rows,cols,gapPenalty);

	for (int i = 1; i < rows; i++){
		for (int j = 1; j < cols; j++){
			if (firstArray[i-1] == secondArray[j-1]){
				penalty = data->match;
			}else{
				penalty = data->missMatch;
			}

			left = 		(*result)[i * cols + (j-1)].value + data->gap;
			up = 		(*result)[(i - 1) * cols + j].value + data->gap;
			diagonal = 	(*result)[(i - 1) * cols + (j-1)].value + penalty;

			//MAX 0
			max = MAX(MAX(left,up),diagonal);
        	(*result)[i * cols + j].value = max;

    		if (up == max) (*result)[i * cols + j].up = true;
    		if (left == max) (*result)[i * cols + j].left = true;
    		if (diagonal == max) (*result)[i * cols + (j)].diagonal = true;


		}
	}
	//printTable(result,rows,cols);
}


body_tuple *getNeedlemanWunschAl(body_cell **table,char * word1,char * word2){
	int x = strlen(word1);
	int y = strlen(word2);
	int cols = strlen(word2) + 1;

	body_tuple *result = malloc(sizeof(body_tuple));
	result->value1 = malloc(sizeof(char)*(strlen(word1) + strlen(word2) +1));
	result->value2 = malloc(sizeof(char)*(strlen(word1) + strlen(word2) +1));

	int pos = 0;

	while (x != 0 || y != 0){
		// printf("POS %d\n", (*table)[x * cols+ y].value);
		// printf("UP %d LEFT %d DIAGONAL %d \n", (*table)[x * cols+ y].up,(*table)[x * cols+ y].left,(*table)[x * cols+ y].diagonal);
		// printf("X : %d Y : %d \n", x,y );
		// sleep(2);
		if ((*table)[x * cols + y].diagonal){
			result->value1[pos] = word1[x-1];
			result->value2[pos] = word2[y-1];
			result->value1[pos+1] = '\0';
			result->value2[pos+1] = '\0';
			x --;
			y --;
		}else if ((*table)[x * cols+ y].up){
			result->value1[pos] = word1[x-1];
			result->value2[pos] = '-';

			result->value1[pos+1] = '\0';
			result->value2[pos+1] = '\0';
			x --;
		}else if ((*table)[x * cols + y].left){
			result->value1[pos] = '-';
			result->value2[pos] = word2[y-1];
			result->value1[pos+1] = '\0';
			result->value2[pos+1] = '\0';
			y --;
		}
		pos ++;
	}

	pos --;
	char temp;
	int i = 0;
	while (i < pos){
		temp = result->value1[pos];
		result->value1[pos] = result->value1[i];
		result->value1[i] = temp;

		temp = result->value2[pos];
		result->value2[pos] = result->value2[i];
		result->value2[i] = temp;

		i ++;
		pos --;
	}

	return result;
}

//====================END NEEDLEMAN BUNCH===============

//====================LINEAR NEEDLEMAN BUNCH===============


int * getPrefixScoring(body_aligment *data,char *word1, char *word2){
	int *result = malloc(sizeof(int)*(strlen(word2)+1));
	int countW1 = strlen(word1)+1;
	int countW2 = strlen(word2)+1;
	int gap = data->gap;
	int match = data->match;
	int missMatch = data->missMatch;
	int left,up,diagonal,penalty,temp;

	for (int i = 0; i < countW2; i++){
		result[i] = i * gap;
	}

	for (int i = 1; i < countW1; i++){
		diagonal = result[0];
		result[0] = i * gap;
		for (int j = 1; j < countW2; j++){
			if (word1[i-1] == word2[j-1]){
				penalty = match;
			}else{
				penalty = missMatch;
			}

			left = 		result[j-1];
			up = 		result[j];

			result[j] = MAX(MAX(left + gap,up + gap),diagonal + penalty);

			diagonal = up;

		}

	}

	return result;

}

int * getSufixScoring(body_aligment *data,char *word1, char *word2){
	char buffer1[strlen(word1) ];
	char buffer2[strlen(word1) ];
	buffer1[strlen(word1) ] = '\0';
	buffer2[strlen(word2) ] = '\0';
	int *result = malloc(sizeof(int)*(strlen(word2)+1));
	int r;

	r = strlen(word1) - 1;
	for (int i = 0; i < strlen(word1); i++){
		buffer1[i] = word1[r];
		r --;
	}

	r = strlen(word2) - 1;
	for (int i = 0; i < strlen(word2); i++){
		buffer2[i] = word2[r];
		r --;
	}

	int *temp = getPrefixScoring(data,buffer1,buffer2);

	r = strlen(word2);
	for (int i = 0; i < strlen(word2)+1; i++){
		result[i] = temp[r];
		r --;
	}

	return result;
}




int getMaxPos(int *prefix,int *sufix,int count){
	int max = 0;
	int pos = -1;
	for (int i = 0; i < count; i++){
		if (max < (prefix[i] + sufix[i]) || pos == -1){
			max = prefix[i] + sufix[i];
			pos = i;
		}
	}
	return pos;
}


body_tuple* getNeedlemanWunschLinear(body_aligment *data,char * word1,char * word2){

	body_tuple * result = NULL;

	if (strlen(word1) == 0){
		result = malloc(sizeof(body_tuple));
		result->value1 = malloc(sizeof(char)*strlen(word2));
		result->value2 = malloc(sizeof(char)*strlen(word2));
		for (int i = 0; i < strlen(word2); i++){
			result->value1[i] = '-';
			result->value2[i] = word2[i];
		}
	}else if (strlen(word2) == 0){
		result = malloc(sizeof(body_tuple));
		result->value1 = malloc(sizeof(char)*strlen(word1));
		result->value2 = malloc(sizeof(char)*strlen(word1));
		for (int i = 0; i < strlen(word1); i++){
			result->value2[i] = '-';
			result->value1[i] = word1[i];
		}
	}else if (strlen(word1) == 1 || strlen(word2) == 1){
		body_cell *table = NULL;
		data->word1 = word1;
		data->word2 = word2;

		getNeedlemanWunsch(&table,data);

		result = getNeedlemanWunschAl(&table,data->word1,data->word2);

	}else{
		int pivotWord1 = strlen(word1) / 2;
		char bufferPrefix[pivotWord1];
		char bufferSufix[strlen(word1)-pivotWord1];
		strncpy(bufferPrefix,word1,pivotWord1);
		bufferPrefix[pivotWord1] = '\0';
		strcpy(bufferSufix,word1 + pivotWord1);

		int *bestSufix = getSufixScoring(data,bufferSufix, word2);
		int *bestPrefix = getPrefixScoring(data,bufferPrefix, word2);

		int pivotWord2 = getMaxPos(bestSufix,bestPrefix,strlen(word2)+1);

		char bufferPrefix2[pivotWord2];
		char bufferSufix2[strlen(word2)-pivotWord2];
		strncpy(bufferPrefix2,word2,pivotWord2);
		bufferPrefix2[pivotWord2] = '\0';
		strcpy(bufferSufix2,word2 + pivotWord2);

		body_tuple *left = getNeedlemanWunschLinear(data,bufferPrefix,bufferPrefix2);

		body_tuple *right = getNeedlemanWunschLinear(data,bufferSufix,bufferSufix2);
		result = malloc(sizeof(body_tuple));
		result->value1 = malloc(sizeof(char)*(strlen(left->value1) + strlen(right->value1)));
		result->value2 = malloc(sizeof(char)*(strlen(left->value2) + strlen(right->value2)));

		strcpy(result->value1,left->value1);
		strcat(result->value1,right->value1);

		strcpy(result->value2,left->value2);
		strcat(result->value2,right->value2);
		free(left);
		free(right);

	}

	return result;
}




//====================END LINEAR NEEDLEMAN BUNCH===============


char *generateRandomChar(int size){
	int val;
	char *buffer = malloc(sizeof(char) * (size+1));


	for (int i = 0; i < size; i++){
		val = rand() %4;
		buffer[i] = alphabet[val];
	}
	buffer[size] = '\0';
	return buffer;
}

void eventButtonLoad(){
	int word1Size,word2Size;
	body_cell *result = NULL;
	int *resultL = NULL;
	char *resultAL = NULL;
	char *format;
	char *markup;

	body_aligment *data = malloc(sizeof(body_aligment));

	word1Size = gtk_spin_button_get_value(size_word1);
	word2Size = gtk_spin_button_get_value(size_word2);
	data->word1 = generateRandomChar(word1Size);
	data->word2 = generateRandomChar(word2Size);

	data->match = gtk_spin_button_get_value(spinMatch);
	data->missMatch = gtk_spin_button_get_value(spinMissmatch);
	data->gap = gtk_spin_button_get_value(spinGap);


	//////////////////////
	memoryCount = 0;
	startNW = clock();

	getNeedlemanWunsch(&result,data);

	printTable(&result,word1Size+1,word2Size+1);

	body_tuple* test = getNeedlemanWunschAl(&result,data->word1 ,data->word2);
	printf("val1 %s\n", test->value1);
	printf("val2 %s\n", test->value2);

	endNW = clock();

	printf("====================================================================\n");

	double cpu_time_used = ((double) (endNW - startNW)) / CLOCKS_PER_SEC;
	char buffer[16];
	sprintf(buffer,"%f s",cpu_time_used);

	gtk_label_set_text (Label_timeNW,buffer);

	sprintf(buffer,"%f mb's",memoryCount);
	
	gtk_label_set_text (Label_MemoryNW,buffer);

	//////////////////////

	memoryCount = 0;
	startNWL = clock();

	body_tuple* test2 = getNeedlemanWunschLinear(data,data->word1,data->word2);

	endNWL = clock();

	printf("val1 %s\n", test2->value1);
	printf("val2 %s\n", test2->value2);

	cpu_time_used = ((double) (endNWL - startNWL)) / CLOCKS_PER_SEC;
	buffer[16];
	sprintf(buffer,"%f s",cpu_time_used);
	
	gtk_label_set_text (Label_timeNWL,buffer);

	sprintf(buffer,"%f mb's",memoryCount);
	
	gtk_label_set_text (Label_MemoryNWL,buffer);

	printf("FIN\n");

	//////////////////////

	//show aligments NeedlemanWunschAl

	format = "<span font_weight='bold' background='#0A0' letter_spacing='50' font='Roboto 14'>\%s</span>";
	markup = g_markup_printf_escaped (format, addTabs(test->value1,word1Size,word2Size));
	gtk_label_set_markup (AligmentFirst1, markup);
	g_free (markup);

	format = "<span font_weight='bold' background='#CC0' letter_spacing='50' font='Roboto 14'>\%s</span>";
	markup = g_markup_printf_escaped (format,addTabs(test->value2,word1Size,word2Size));
	gtk_label_set_markup (AligmentSecond1, markup);
	g_free (markup);

	format = "<span font_weight='bold' background='#A0A' letter_spacing='50' font='Roboto 14'>\%s</span>";
	markup = g_markup_printf_escaped (format, getScoreAligment(test->value1,test->value2,data->match,data->missMatch,data->gap));
	gtk_label_set_markup (AligmentScore1, markup);
	g_free (markup);

	format = "<span font_weight='bold' foreground='#FFF' background='#00B'  font='Roboto 30'>\%d</span>";
	markup = g_markup_printf_escaped (format,getScoring(test->value1,test->value2,data->match,data->missMatch,data->gap));
	gtk_label_set_markup (Scoring1, markup);
	g_free (markup);

	//show aligments NeedlemanWunschLinear

	format = "<span font_weight='bold' background='#0A0' letter_spacing='50' font='Roboto 14'>\%s</span>";
	markup = g_markup_printf_escaped (format, addTabs(test2->value1,word1Size,word2Size));
	gtk_label_set_markup (AligmentFirst, markup);
	g_free (markup);

	format = "<span font_weight='bold' background='#CC0' letter_spacing='50' font='Roboto 14'>\%s</span>";
	markup = g_markup_printf_escaped (format, addTabs(test2->value2,word1Size,word2Size));
	gtk_label_set_markup (AligmentSecond, markup);
	g_free (markup);

	format = "<span font_weight='bold' background='#A0A' letter_spacing='50' font='Roboto 14'>\%s</span>";
	markup = g_markup_printf_escaped (format, getScoreAligment(test2->value1,test2->value2,data->match,data->missMatch,data->gap));
	gtk_label_set_markup (AligmentScore, markup);
	g_free (markup);

	format = "<span font_weight='bold' foreground='#FFF' background='#00B'  font='Roboto 30'>\%d</span>";
	markup = g_markup_printf_escaped (format,getScoring(test2->value1,test2->value2,data->match,data->missMatch,data->gap));
	gtk_label_set_markup (Scoring, markup);
	g_free (markup);

	//char * a = "TCGGC";
	//char * b = "ATTGTGATCC";
	//int *result2 = getSufixScoring(data,,);

	//for (int i = 0; i < strlen(b) + 1; i++){
	//	printf("Val %d\n", result2[i]);
	//}



}


int main(int argc, char const *argv[]){

	srand(time(NULL));

	 gtk_init(&argc, &argv);

	 builder = gtk_builder_new();

	 gtk_builder_add_from_file (builder, "Glade/mainWindow.glade", NULL);

	 gtk_builder_connect_signals(builder, NULL);

	 spinGap = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Gap"));
   spinMatch = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Match"));
   spinMissmatch = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Missmatch"));
	 size_word1 = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Size_First"));
   size_word2 = GTK_WIDGET(gtk_builder_get_object(builder, "Spinbutton_Size_Second"));
	 AligmentFirst1 =  GTK_LABEL(gtk_builder_get_object(builder, "Alingment_First1"));
	 AligmentSecond1 =  GTK_LABEL(gtk_builder_get_object(builder,"Alingment_Second1"));
	 AligmentScore1 =  GTK_LABEL(gtk_builder_get_object(builder,"Scoring_Aligment1"));
	 Scoring1 =  GTK_LABEL(gtk_builder_get_object(builder,"Scoring1"));
	 AligmentFirst =  GTK_LABEL(gtk_builder_get_object(builder, "Alingment_First"));
	 AligmentSecond =  GTK_LABEL(gtk_builder_get_object(builder,"Alingment_Second"));
	 AligmentScore =  GTK_LABEL(gtk_builder_get_object(builder,"Scoring_Aligment"));
	 Scoring =  GTK_LABEL(gtk_builder_get_object(builder,"Scoring"));

	 Label_timeNW =  GTK_LABEL(gtk_builder_get_object(builder, "Label_timeNW"));
	 Label_timeNWL =  GTK_LABEL(gtk_builder_get_object(builder,"Label_timeNWL"));

	 Label_MemoryNW =  GTK_LABEL(gtk_builder_get_object(builder, "Label_MemoryNW"));
	 Label_MemoryNWL =  GTK_LABEL(gtk_builder_get_object(builder,"Label_MemoryNWL"));


	 mainWindow = GTK_WIDGET(gtk_builder_get_object(builder, "Window"));

	 g_object_unref(builder);
	 gtk_widget_show_all (mainWindow);
	 gtk_main();

	//eventButtonLoad();

	return 0;
}
