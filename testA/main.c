#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>
//========GLOBAL=======

GtkBuilder      *builder;
GtkWidget       *mainWindow;
GtkWidget       *spinMatch;
GtkWidget       *spinMissmatch;
GtkWidget       *spinGap;
GtkWidget       *buttonGenerate;




gchar    *firstString;
gchar    *secondString;
gint 	matchValue = 0;
gint 	missMatchValue = 0;
gint 	gapValue = 0;

//===============


void saveFile(){

  char * fileName;
  fileName = gtk_entry_get_text(textBoxFile);

  //default name
  if(strcmp(fileName, "") == 0){
        fileName = "default.txt";
  }

  FILE *f = fopen(fileName, "w");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }

  char totalGenes[10];
  sprintf(totalGenes,"%d",totalSelectedGenes);
  fprintf(f,totalGenes);
  fprintf(f,"\n");

  while (current!=NULL){

    fprintf(f,current->name);
    fprintf(f,"\n");
    fprintf(f,current->description);
    fprintf(f,"\n");

    current = current->next;

  }

  for (int i = 0; i < totalSelectedGenes;i ++){
      for (int j = 0; j < totalSelectedGenes; j++)
      {
          char buffer[10];
          sprintf(buffer,"%.2f",table[i][j]);
          //printf("%d ",table[i][j]);
          fprintf(f,buffer);
          fprintf(f," ");
      }
      fprintf(f,"\n");
  }

  fclose(f);

}



int main(int argc, char const *argv[])
{
	gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/test.glade", NULL);
    gtk_builder_connect_signals(builder, NULL);

    mainWindow = GTK_WIDGET(gtk_builder_get_object(builder, "Window"));
    ScrolledWindow = GTK_WIDGET(gtk_builder_get_object(builder, "ScrolledWindow"));
    ScrolledWindow_Table = GTK_WIDGET(gtk_builder_get_object(builder, "ScrolledWindow_Table"));
    ButtonBox_Genes = GTK_WIDGET(gtk_builder_get_object(builder, "ButtonBox_Genes"));

    textBoxName = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_name"));
    textBoxDescription = GTK_WIDGET(gtk_builder_get_object(builder, "TextBox_description"));

    g_signal_connect (mainWindow, "delete_event", gtk_main_quit, NULL);




	return 0;
}